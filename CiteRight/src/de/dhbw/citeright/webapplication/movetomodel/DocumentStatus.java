package de.dhbw.citeright.webapplication.movetomodel;

/**
 * This Enum lists the possible states of a document.
 * 
 * @author Jan (i13015)
 */
public enum DocumentStatus {
	IN_QUEUE, AUDIT_RUNNING, AUDIT_FINISHED
}
