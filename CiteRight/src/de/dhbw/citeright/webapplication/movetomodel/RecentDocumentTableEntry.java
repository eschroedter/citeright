package de.dhbw.citeright.webapplication.movetomodel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Holds the values of a table row in the recent handed in documents-table at
 * the dashboard.
 * 
 * @author Jan (i13015)
 */
public class RecentDocumentTableEntry {
	public final Date dateOfEntry;
	public final String studentNameAndMatNr;
	public final DocumentStatus documentStatus;
	public final int plagiarismDegree;

	/**
	 * Initializes the table entry.
	 * 
	 * @param dateOfEntry
	 *            Date object containing the date when the file was handed in.
	 * @param studentNameAndMatNr
	 *            For now a string containing the student's name and
	 *            matriculation number.
	 * @param documentStatus
	 *            The current audit status of the document.
	 * @param plagiarismDegree
	 *            The plagiarism degree of the document after audit has
	 *            finished.
	 */
	public RecentDocumentTableEntry(Date dateOfEntry,
			String studentNameAndMatNr, DocumentStatus documentStatus,
			int plagiarismDegree) {
		this.dateOfEntry = dateOfEntry;
		this.studentNameAndMatNr = studentNameAndMatNr;
		this.documentStatus = documentStatus;
		this.plagiarismDegree = plagiarismDegree;
	}

	/**
	 * Outputs the stored data for debugging purposes.
	 * @return A String containing all attributes formatted.
	 */
	@Override
	public String toString() {
		DateFormat formatter = new SimpleDateFormat();

		// Format the DocumentStatus so that it is human-readable.
		String documentStatusOutput = "";

		switch (this.documentStatus) {
		case IN_QUEUE:
			documentStatusOutput = "In Warteschlange zur Prüfung";
			break;
		case AUDIT_RUNNING:
			documentStatusOutput = "Prüfung läuft";
			break;
		case AUDIT_FINISHED:
			documentStatusOutput = "Prüfung abgeschlossen";
			break;
		}

		// Format the plagiarism degree to be human-readable.
		String plagiarismDegreeOutput = String.valueOf(this.plagiarismDegree)
				+ " %";

		return formatter.format(this.dateOfEntry) + "; "
				+ this.studentNameAndMatNr + "; " + documentStatusOutput
				+ "; " + plagiarismDegreeOutput;
	}
}
