package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.webapplication.view.pages.StudentUploadView;

/**
 * This listener is called when a user hands in an document via url input.
 * 
 * @author Kai Holzer
 *
 */
@SuppressWarnings("serial")
public class URLUploadListener implements Button.ClickListener {

	private StudentUploadView studentUploadView;

	/**
	 * Initializes the listener for a given View.
	 * 
	 * @param studentUploadView The view where the listener is used.
	 */
	public URLUploadListener(StudentUploadView studentUploadView) {
		this.studentUploadView = studentUploadView;
	}

	/**
	 * This method will be executed, when a button is clicked. It will check if
	 * a given URL is correct.
	 * 
	 * @param event An Object holding details to the event.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		// As there is only one button, which is the "send form" button, we
		// won't need to check which button has been pressed.
		if (studentUploadView.formFilledOut()
				&& !studentUploadView.getUploadURL().equals("")) {
			// TODO Prüfen, ob URL korrekt (z.B. matcht auf Regex und
			// HTTP-Statuscode == 2XX || == 3XX)
			String uploadURL = studentUploadView.getUploadURL();
			Notification.show("URL wurde eingereicht: " + uploadURL);
		} else {
			Notification.show("Es sind nicht alle Felder ausgefüllt",
					Type.WARNING_MESSAGE);
		}
	}

}
