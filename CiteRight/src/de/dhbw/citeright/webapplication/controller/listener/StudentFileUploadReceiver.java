package de.dhbw.citeright.webapplication.controller.listener;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Date;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

import de.dhbw.citeright.essentials.db.File;
import de.dhbw.citeright.essentials.db.Folder;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.FileService;
import de.dhbw.citeright.essentials.db.service.FolderService;
import de.dhbw.citeright.webapplication.view.pages.StudentUploadView;

/**
 * Receives a file from the StudentUploadView and saves it it the database.
 * 
 * @author Kai Holzer
 *
 */
@SuppressWarnings("serial")
public class StudentFileUploadReceiver implements SucceededListener, FailedListener,
		Receiver {
	private String filename;
	private String mimeType;
	private ByteArrayOutputStream output;
	private Timestamp uploadTime;
	private StudentUploadView studentUploadView;

	public StudentFileUploadReceiver(StudentUploadView studentUploadView) {
		this.studentUploadView = studentUploadView;
	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		this.filename = filename;
		this.mimeType = mimeType;
		this.uploadTime = new Timestamp(new Date().getTime());

		output = new ByteArrayOutputStream();

		return output;
	}

	@Override
	public void uploadFailed(FailedEvent event) {
		Notification.show("Upload fehlgeschlagen",
				event.getReason().toString(), Type.ERROR_MESSAGE);
	}
	
	@Override
	public void uploadSucceeded(SucceededEvent event) {
		if (!studentUploadView.formFilledOut()) {
			Notification.show("Bitte füllen Sie alle Pflichtfelder aus.",
					Type.ERROR_MESSAGE);
			return;
		}
		
		if (filename.length() == 0) {
			Notification.show("Schreiben in Datenbank fehlgeschlagen",
					Type.ERROR_MESSAGE);
			return;
		}
		
		FolderService folderService = new FolderService(HibernateUtils.getSessionFactory());
		Folder folder = folderService.getFolderByToken(studentUploadView.getTokenCode());
		if(folder != null && folder.getId() > 0){
			File file = new File(filename, mimeType, ((ByteArrayOutputStream)output).toByteArray(), uploadTime, folder.getId());
			
			FileService fileService = new FileService(HibernateUtils.getSessionFactory());
			if(fileService.create(file) > 0){
				Notification.show("Datei wurde erfolgreich hochgeladen");
			}else{
				Notification.show("Schreiben in Datenbank fehlgeschlagen",
						Type.ERROR_MESSAGE);
			}
		}else{
			Notification.show("Invalider Token.",
					Type.ERROR_MESSAGE);
		}
		

	}

}
