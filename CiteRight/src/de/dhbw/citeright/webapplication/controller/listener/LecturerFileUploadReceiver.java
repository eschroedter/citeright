package de.dhbw.citeright.webapplication.controller.listener;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Date;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

import de.dhbw.citeright.essentials.db.File;
import de.dhbw.citeright.essentials.db.Folder;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.FileService;
import de.dhbw.citeright.essentials.db.service.FolderService;
import de.dhbw.citeright.webapplication.view.pages.LecturerUploadView;
import de.dhbw.citeright.webapplication.view.pages.StudentUploadView;

/**
 * Receives a file from the LecturerUploadView and saves it it the database.
 * 
 * @author Kai Holzer
 *
 */
@SuppressWarnings("serial")
public class LecturerFileUploadReceiver implements SucceededListener,
		FailedListener, Receiver {

	private String filename;
	private String mimeType;
	private ByteArrayOutputStream output;
	private Timestamp uploadTime;
	private LecturerUploadView lecturerUploadView;

	public LecturerFileUploadReceiver(LecturerUploadView lecturerUploadView) {
		this.lecturerUploadView = lecturerUploadView;
	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		this.filename = filename;
		this.mimeType = mimeType;
		this.uploadTime = new Timestamp(new Date().getTime());

		output = new ByteArrayOutputStream();

		return output;
	}

	@Override
	public void uploadFailed(FailedEvent event) {
		Notification.show("Upload fehlgeschlagen",
				event.getReason().toString(), Type.ERROR_MESSAGE);
	}

	@Override
	public void uploadSucceeded(SucceededEvent event) {
		if (filename.length() == 0) {
			Notification.show("Schreiben in Datenbank fehlgeschlagen",
					Type.ERROR_MESSAGE);
			return;
		}
		Notification.show("Datei wurde in Raum "+ lecturerUploadView.getSelectedRoom() + " hochgeladen");
	}

}
