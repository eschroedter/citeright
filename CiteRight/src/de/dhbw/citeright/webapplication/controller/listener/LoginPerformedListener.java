package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.StartView;

/**
 * This listener is called, when a user tries to log in
 * 
 * @author Kai Holzer
 */
@SuppressWarnings("serial")
public class LoginPerformedListener implements Button.ClickListener {
	private StartView startView;
	private UserService userService;

	/**
	 * Initializes the listener for a given View.
	 * 
	 * @param studentUploadView
	 *            The view where the listener is used.
	 */
	public LoginPerformedListener(StartView startView) {
		this.startView = startView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * This method will be executed, when a button is clicked. It will check the
	 * given username and password.
	 * 
	 * @param event
	 *            An Object holding details to the event.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String email = startView.getEmail();
		String password = startView.getPassword();

		if (userService.login(email, password)) {
			Notification.show("Login erfolgreich", Type.TRAY_NOTIFICATION);
			startView.successfullLogin();
		}else {
			Notification.show("Fehlerhafte Daten eingegeben",
					Type.WARNING_MESSAGE);
			startView.clearPasswordField();
		}

	}

}
