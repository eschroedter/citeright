package de.dhbw.citeright.webapplication.view;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

import de.dhbw.citeright.webapplication.view.pages.DashboardView;
import de.dhbw.citeright.webapplication.view.pages.LecturerUploadView;
//TODO: Commented out for building. LecturerUploadView doesn't exist!  ---Florian
//import de.dhbw.citeright.webapplication.view.pages.LecturerUploadView;
import de.dhbw.citeright.webapplication.view.pages.RegistrationView;
import de.dhbw.citeright.webapplication.view.pages.StartView;
import de.dhbw.citeright.webapplication.view.pages.StudentTokenView;
import de.dhbw.citeright.webapplication.view.pages.StudentUploadView;

/**
 * is the startpoint of the application where the Views gets added
 * 
 * @author Kai Holzer
 */
@SuppressWarnings("serial")
@Theme("citeright")
public class CiterightUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = CiterightUI.class)
	public static class Servlet extends VaadinServlet {
	}

	/**
	 * initialize the UI
	 */
	@Override
	protected void init(VaadinRequest request) {
		Navigator navigator = new Navigator(this, this);

		navigator.addView(StartView.NAME, new StartView());
		navigator.addView(StudentTokenView.NAME, new StudentTokenView());
		navigator.addView(RegistrationView.NAME, new RegistrationView());
		navigator.addView(DashboardView.NAME, new DashboardView());
		navigator.addView(StudentUploadView.NAME, new StudentUploadView());	
		navigator.addView(LecturerUploadView.NAME, new LecturerUploadView());
	}

}