package de.dhbw.citeright.webapplication.view.layout;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.VerticalLayout;

/**
 * This is the layout for all Views before a User was logged in.
 * 
 * @author Kai Holzer
 */
@SuppressWarnings("serial")
public class PreLoginLayout extends VerticalLayout implements View {
	protected VerticalLayout layout;
	protected VerticalLayout content;

	/**
	 * Initializes the Layout
	 */
	public PreLoginLayout() {
		content = new VerticalLayout();
		content.setMargin(true);
		content.setSpacing(true);

		layout = new VerticalLayout();

		addComponent(layout);
		
		setMenuBar();
	}

	/**
	 * Sets a menubar at the top of the page
	 */
	private void setMenuBar() {
		MenuBar menu = new MenuBar();
		menu.setWidth(100, UNITS_PERCENTAGE);
		menu.addItem("", new ThemeResource("images/CiteRightWhiteSmall.png"),
				menuCommand);
		menu.addItem("Studentenupload", menuCommand);
		menu.addItem("Registrieren", menuCommand);

		layout.addComponent(menu);
	}

	/**
	 * Is called when a button in the menubar is pressed
	 */
	MenuBar.Command menuCommand = new MenuBar.Command() {

		/**
		 * Overried the standard menuSelected method of MenuBar with our
		 * navigation procedures for the Views.
		 * 
		 * @param selectedItem
		 *            An Object holding information on the selected item in the
		 *            menu bar.
		 */
		@Override
		public void menuSelected(MenuItem selectedItem) {
			switch (selectedItem.getText()) {
			case "Studentenupload":
				getUI().getNavigator().navigateTo("studenttoken");
				break;
			case "Registrieren":
				getUI().getNavigator().navigateTo("registration");
			case "":
				getUI().getNavigator().navigateTo("");
				break;
			}
		}
	};

	/**
	 * Is called when a view was loaded. Informs the parent class about the
	 * access. If a user is already logged in, he will be redirected to the
	 * dashboard to prevent the display of the login page.
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	@Override
	public void enter(ViewChangeEvent event) {

		String loggedUser = String.valueOf(getSession()
				.getAttribute("username"));
		if (!loggedUser.equals("null")) {
			getUI().getNavigator().navigateTo("dashboard");
		}
	}

}
