package de.dhbw.citeright.webapplication.view.layout;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.MenuBar.MenuItem;

import de.dhbw.citeright.webapplication.view.pages.DashboardView;
import de.dhbw.citeright.webapplication.view.pages.LecturerUploadView;
//TODO: Commented out for building. LecturerUploadView doesn't exist! You may commit but NOT push files with errors. ---Florian
//import de.dhbw.citeright.webapplication.view.pages.LecturerUploadView;
import de.dhbw.citeright.webapplication.view.pages.StartView;

/**
 * This is the layout for all Views when a User has logged in
 * 
 * @author Kai Holzer
 */
@SuppressWarnings("serial")
public class MemberAreaLayout extends VerticalLayout implements View {
	protected VerticalLayout layout;
	protected VerticalLayout content;

	public MemberAreaLayout() {
		layout = new VerticalLayout();
		content = new VerticalLayout();
		content.setMargin(true);
		content.setSpacing(true);

		setMenuBar();
		layout.addComponent(content);
		addComponent(layout);
	}

	/**
	 * sets a menubar at the top of the page
	 */
	private void setMenuBar() {
		MenuBar menu = new MenuBar();
		menu.setWidth(100, UNITS_PERCENTAGE);
		menu.addItem("", new ThemeResource("images/CiteRightWhiteSmall.png"),
				menuCommand);
		menu.addItem("Dashboard", menuCommand);
		menu.addItem("Räume", menuCommand);
		menu.addItem("Einstellungen", menuCommand);
		menu.addItem("Upload", menuCommand);
		menu.addItem("Logout", menuCommand);
		layout.addComponent(menu);
	}

	/**
	 * gets called if a button in the menubar gets pressed
	 */
	MenuBar.Command menuCommand = new MenuBar.Command() {
		@Override
		public void menuSelected(MenuItem selectedItem) {
			switch (selectedItem.getText()) {
			case "":
				getUI().getNavigator().navigateTo(DashboardView.NAME);
				break;
			case "Dashboard":
				getUI().getNavigator().navigateTo(DashboardView.NAME);
				break;
			case "Räume":
				getUI().getNavigator().navigateTo("");
				break;
			case "Einstellungen":
				getUI().getNavigator().navigateTo("");
				break;
			case "Logout": 
				Notification.show("Logout erfolgreich", Type.TRAY_NOTIFICATION);
				getSession().setAttribute("username", null);
				getUI().getNavigator().navigateTo(StartView.NAME);
				break;
			case "Upload":
				getUI().getNavigator().navigateTo(LecturerUploadView.NAME);
				break;
			}
		}
	};

	/**
	 * gets called if a view did load
	 */
	@Override
	public void enter(ViewChangeEvent event) {
		String loggedUser = String.valueOf(getSession()
				.getAttribute("username"));
		if (loggedUser.equals("null")) {
			getUI().getNavigator().navigateTo("");
		}
	}
}
