package de.dhbw.citeright.webapplication.view.pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.webapplication.movetomodel.DocumentStatus;
import de.dhbw.citeright.webapplication.movetomodel.RecentDocumentTableEntry;
import de.dhbw.citeright.webapplication.view.layout.MemberAreaLayout;

/**
 * The view for the dashboard.
 * 
 * @author Jan (i13015)
 */
@SuppressWarnings("serial")
public class DashboardView extends MemberAreaLayout {
	public static final String NAME = "dashboard";

	Table tableLastHandedInDocuments;

	/**
	 * Initializes the View by calling the constructor of the parent class.
	 */
	public DashboardView() {
		super();

		initalizeDashboard();
	}

	/**
	 * Initializes the dashboard with a welcome screen and a table of documents,
	 * that have recently been handed in.
	 */
	private void initalizeDashboard() {

		// Divide the Dashboard with a horizontal layout in an information text
		// and the table of recent handed in documents.
		VerticalLayout dashboardVertical = new VerticalLayout();

		Label informationText = new Label();
		informationText.setValue("Herzlich Willkommen beim Plagiatsprüfer.");
		// informationText.setWidth("100%");
		dashboardVertical.addComponent(informationText);

		tableLastHandedInDocuments = new Table("Aktuell eingereichte Dokumente");

		// Add headers to Table
		tableLastHandedInDocuments.addContainerProperty("Datum", String.class,
				null);
		tableLastHandedInDocuments.addContainerProperty("Student",
				String.class, null);
		tableLastHandedInDocuments.addContainerProperty("Status", String.class,
				null);
		tableLastHandedInDocuments.addContainerProperty("Plagiatsmaß",
				String.class, null);

		// Load the contents of the table
		loadRecentDocumentsInTable(tableLastHandedInDocuments);

		// A hack to set the table height to their acutal height according to
		// the number of elements in the table.
		// http://stackoverflow.com/a/13712120
		tableLastHandedInDocuments.setPageLength(tableLastHandedInDocuments
				.getItemIds().size() + 1);

		dashboardVertical.addComponent(tableLastHandedInDocuments);

		content.addComponent(dashboardVertical);
	}

	/**
	 * Loads recent handed in documents out of the database and adds their
	 * metadata to the table given in the parameter.
	 * 
	 * @param tableObject
	 *            The table to add the rows into.
	 */
	private void loadRecentDocumentsInTable(Table tableObject) {
		// TODO Aus der Datenbank holen, wenn Model weiter vorangeschritten ist.
		// Hier nur Dummyeinträge.
		List<RecentDocumentTableEntry> tableEntries = getDummyEntries();

		tableObject.clear();

		int tableCellId = 1;

		for (RecentDocumentTableEntry currentEntry : tableEntries) {
			tableObject.addItem(formatTableRow(currentEntry), tableCellId++);
		}
	}

	/**
	 * Formats a given table entry so that the values are human readable.
	 * 
	 * @param tableEntry
	 *            The entry that shall be formatted.
	 * @return An array of the cells as their individual objects.
	 */
	private Object[] formatTableRow(RecentDocumentTableEntry tableEntry) {
		DateFormat formatter = new SimpleDateFormat();

		// Format the DocumentStatus so that it is human-readable.
		String documentStatusOutput = "";

		switch (tableEntry.documentStatus) {
		case IN_QUEUE:
			documentStatusOutput = "In Warteschlange zur Prüfung";
			break;
		case AUDIT_RUNNING:
			documentStatusOutput = "Prüfung läuft";
			break;
		case AUDIT_FINISHED:
			documentStatusOutput = "Prüfung abgeschlossen";
			break;
		}

		// Format the plagiarism degree to be human-readable.
		String plagiarismDegreeOutput = String
				.valueOf(tableEntry.plagiarismDegree) + " %";

		// Only show plagiarism degree if audit is finished.
		if (tableEntry.documentStatus != DocumentStatus.AUDIT_FINISHED) {
			plagiarismDegreeOutput = "";
		}

		return new Object[] { formatter.format(tableEntry.dateOfEntry),
				tableEntry.studentNameAndMatNr, documentStatusOutput,
				plagiarismDegreeOutput };
	}

	/**
	 * A method that creates dummy data for debugging purposes.
	 * 
	 * @return A List of dummy table entries.
	 */
	private List<RecentDocumentTableEntry> getDummyEntries() {
		// TODO Diese Methode muss unbedingt entfernt werden, wenn Model/DB
		// fertig sind.
		Calendar cal = Calendar.getInstance();
		List<RecentDocumentTableEntry> returnList = new LinkedList<RecentDocumentTableEntry>();

		// Entry #1
		cal.clear();
		cal.set(2015, 05, 01, 16, 00);
		returnList.add(new RecentDocumentTableEntry(cal.getTime(),
				"Student 1 (2376420)", DocumentStatus.IN_QUEUE, 0));

		// Entry #2
		cal.clear();
		cal.set(2015, 05, 01, 15, 30);
		returnList.add(new RecentDocumentTableEntry(cal.getTime(),
				"Student 2 (9620687)", DocumentStatus.IN_QUEUE, 0));

		// Entry #3
		cal.clear();
		cal.set(2015, 05, 01, 15, 00);
		returnList.add(new RecentDocumentTableEntry(cal.getTime(),
				"Student 3 (8395748)", DocumentStatus.IN_QUEUE, 0));

		return returnList;
	}

	/**
	 * Is called when the View has been loaded
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);
	}

}
