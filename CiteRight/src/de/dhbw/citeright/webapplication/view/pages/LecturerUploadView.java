package de.dhbw.citeright.webapplication.view.pages;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.webapplication.controller.listener.LecturerFileUploadReceiver;
import de.dhbw.citeright.webapplication.view.layout.MemberAreaLayout;

@SuppressWarnings("serial")
public class LecturerUploadView extends MemberAreaLayout{
	public static final String NAME = "lecturerupload";
	
	private LecturerFileUploadReceiver uploadReceiver;
	private NativeSelect rooms;
	
	
	public LecturerUploadView() {
		super();
		
		initializeLecturerUpload();
		
		layout.addComponent(content);
	}
	
	
	
	private void initializeLecturerUpload() {
		rooms = new NativeSelect("Raum auswählen");
		        
		rooms.addItems("Raum1", "Raum2", "Raum3");

		rooms.setNullSelectionAllowed(false);
		rooms.setMultiSelect(false);
		rooms.setWidth("300px");
		
		content.addComponent(rooms);
		
		uploadReceiver = new LecturerFileUploadReceiver(this);
		Upload upload = new Upload("Bitte Datei auswählen", uploadReceiver);
		upload.addSucceededListener(uploadReceiver);
		upload.setButtonCaption("Dokument einreichen");
		
		content.addComponent(upload);

	
		
	}
	
	/**
	 * Returns the current marked room
	 * @return the name of the marked room
	 */
	public String getSelectedRoom() {
		return (String) rooms.getValue();
	}



	/**
	 * Is called when a view was loaded. Informs the parent class about the
	 * access and sets the focus to the tokenCode field.
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);

	}
}
