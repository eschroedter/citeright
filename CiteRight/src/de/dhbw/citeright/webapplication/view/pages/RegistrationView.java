package de.dhbw.citeright.webapplication.view.pages;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;

import de.dhbw.citeright.webapplication.controller.listener.RegistrationPerformedListener;
import de.dhbw.citeright.webapplication.view.layout.PreLoginLayout;

/**
 * The registration view
 * 
 * @author Kai Holzer
 */
@SuppressWarnings("serial")
public class RegistrationView extends PreLoginLayout {
	public static final String NAME = "registration";

	private TextField firstName;
	private TextField lastName;
	private TextField email;
	private PasswordField password;
	private PasswordField passwordRepeat;

	/**
	 * Initializes the registration view.
	 */
	public RegistrationView() {
		super();
		initializeRegistrationForm();
		
		layout.addComponent(content);
	}

	/**
	 * Initializes the registration form and it's elements.
	 */
	private void initializeRegistrationForm() {

		FormLayout registrationForm = new FormLayout();
		registrationForm.setCaption("Geben Sie hier Ihre Daten ein");

		// Create the textfields and button and set their labels.
		firstName = new TextField("Vorname:");
		firstName.setRequired(true);
		
		lastName = new TextField("Nachname:");
		lastName.setRequired(true);
		
		email = new TextField("Email:");
		email.setRequired(true);

		password = new PasswordField("Passwort");
		password.setRequired(true);
		
		passwordRepeat = new PasswordField("Passwort wiederholen:");
		passwordRepeat.setRequired(true);
		
		// TODO Warum ist das nicht auch eine Klassenvariable?
		Button sendRegistration = new Button("registrieren");
		sendRegistration.addClickListener(new RegistrationPerformedListener(
				this));
		
		// Add form fields to the registrationForm
		registrationForm.addComponent(firstName);
		registrationForm.addComponent(lastName);
		registrationForm.addComponent(email);
		registrationForm.addComponent(password);
		registrationForm.addComponent(passwordRepeat);
		registrationForm.addComponent(sendRegistration);

		// Add registrationForm to the View.
		content.addComponent(registrationForm);
	}

	/**
	 * Will return the current value of the firstName-field.
	 * 
	 * @return firstName as entered in the textfield.
	 */
	public String getFirstName() {
		return firstName.getValue();
	}

	/**
	 * Will return the current value of the lastName-field.
	 * 
	 * @return lastName as entered in the textfield.
	 */
	public String getLastName() {
		return lastName.getValue();
	}

	/**
	 * Will return the current value of the email-field.
	 * 
	 * @return e-mail as entered in the textfield.
	 */
	public String getEmail() {
		return email.getValue();
	}

	/**
	 * Will return the current value of the password-field.
	 * 
	 * @return password as entered in the textfield.
	 */
	public String getPassword() {
		return password.getValue();
	}

	/**
	 * Will return the current value of the passwordRepeat-field.
	 * 
	 * @return repeated password as entered in the textfield.
	 */
	public String getPasswordRepeat() {
		return passwordRepeat.getValue();
	}

	/**
	 * Is called when a view was loaded. Will enter the view and set the focus
	 * on the input element of firstName.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);
		firstName.focus();
	}
}
