package de.dhbw.citeright.webapplication.view.pages;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;

import de.dhbw.citeright.webapplication.controller.listener.LoginPerformedListener;
import de.dhbw.citeright.webapplication.view.layout.PreLoginLayout;

/**
 * The start view where a user is redirected to if he isn't logged in.
 * 
 * @author Kai Holzer
 */
@SuppressWarnings("serial")
public class StartView extends PreLoginLayout implements View {
	public static final String NAME = "";
	private TextField email;
	private PasswordField password;
	private Button loginButton;

	/**
	 * Initializes the View and the login form.
	 */
	public StartView() {
		super();

		initializeLogin();

		layout.addComponent(content);
	}

	/**
	 * Sets the textfields for the user login.
	 */
	private void initializeLogin() {
		email = new TextField("Email:");
		email.setWidth("300px");
		email.setRequired(true);
		email.setInputPrompt("max@mustermann.de");
		email.setInvalidAllowed(false);

		password = new PasswordField("Passwort:");
		password.setWidth("300px");
		password.setRequired(true);
		password.setValue("");
		password.setNullRepresentation("");

		loginButton = new Button("Login");
		loginButton.addClickListener(new LoginPerformedListener(this));

		content.addComponent(email);
		content.addComponent(password);
		content.addComponent(loginButton);

	}

	/**
	 * Returns the current value of the username TextField.
	 * 
	 * @return the text in the username textfield.
	 */
	public String getEmail() {
		return email.getValue();
	}

	/**
	 * Returns the current value of the password TextField.
	 * 
	 * @return the password in the password textfield
	 */
	public String getPassword() {
		return password.getValue();
	}

	/**
	 * Is called when the user was logged in successfully. Resets the login form
	 * and redirects the user to the dashboard.
	 */
	public void successfullLogin() {
		getSession().setAttribute("username", email.getValue());
		email.setValue("");
		password.setValue("");
		getUI().getNavigator().navigateTo("dashboard");
	}

	/**
	 * Clears the password textfield and sets the focus on it.
	 */
	public void clearPasswordField() {
		password.setValue("");
		password.focus();
	}

	/**
	 * Is called when a view was loaded. Informs the parent class about the
	 * access and sets the focus to the username field.
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);
		email.focus();
	}
}
