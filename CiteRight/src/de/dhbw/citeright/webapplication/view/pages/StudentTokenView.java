package de.dhbw.citeright.webapplication.view.pages;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import de.dhbw.citeright.webapplication.controller.listener.SendTokencodeListener;
import de.dhbw.citeright.webapplication.view.layout.PreLoginLayout;

/**
 * A View where a student can enter a token to access the right upload page for
 * a certain room.
 * 
 * @author Kai Holzer
 */
@SuppressWarnings("serial")
public class StudentTokenView extends PreLoginLayout implements View {
	public static final String NAME = "studenttoken";

	private TextField tokenCode;

	/**
	 * Initializes the View and it's form.
	 */
	public StudentTokenView() {
		super();
		initializeTokenForm();
		
		layout.addComponent(content);
	}

	/**
	 * Initializes textfield and button to send a token.
	 */
	private void initializeTokenForm() {

		tokenCode = new TextField("Zugangscode");
		tokenCode.setInputPrompt("z.B. xSa3rT4e3D");

		Button sendButton = new Button("absenden");
		sendButton.addClickListener(new SendTokencodeListener(this));

		FormLayout tokenForm = new FormLayout();
		tokenForm.setWidth("300px");
		tokenForm.setCaption("Geben Sie hier Ihren Zugangscode ein");
		tokenForm.addComponent(tokenCode);
		tokenForm.addComponent(sendButton);

		content.addComponent(tokenForm);
	}

	/**
	 * Returns the current value of the tokenCode TextField.
	 * 
	 * @return the token in the tokenCode textfield
	 */
	public String getTokenCode() {
		return tokenCode.getValue();
	}

	/**
	 * Resets the token input field and redirects the user to the student upload
	 * page for the room.
	 */
	public void gotoUpload() {
		getUI().getNavigator().navigateTo("studentupload/" + getTokenCode());
		tokenCode.setValue("");
	}

	/**
	 * Is called when a view was loaded. Informs the parent class about the
	 * access and sets the focus to the tokenCode field.
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);
		tokenCode.focus();
	}

}
