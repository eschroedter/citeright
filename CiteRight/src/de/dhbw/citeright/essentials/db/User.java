package de.dhbw.citeright.essentials.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * The representation of the User Entity
 * 
 * @author Torsten Hopf
 *
 */
@Entity
@Table(name = "User")
public class User {
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	private Integer id;
	@Column(name = "FirstName")
	private String firstName;
	@Column(name = "LastName", nullable = false)
	private String lastName;
	@Column(name = "Email", unique = true, nullable = false)
	private String email;
	@Column(name = "Password", nullable = false)
	private String password;
	@Column(name = "Role", nullable = false)
	private Integer roleId;

	/**
	 * Creates a User object with the given information
	 * 
	 * @param id
	 *            the Id of the User
	 * @param firstName
	 *            the first name of the User
	 * @param lastName
	 *            the last name of the User
	 * @param email
	 *            the email of the User
	 * @param password
	 *            the password of the User
	 */
	public User(int id, String firstName, String lastName, String email,
			String password) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	/**
	 * Creates a User object with the given information
	 * 
	 * @param firstName
	 *            the first name of the User
	 * @param lastName
	 *            the last name of the User
	 * @param email
	 *            the email of the User
	 * @param password
	 *            the password of the User
	 */
	public User(String firstName, String lastName, String email,
			String password, int roleId) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.roleId = roleId;
	}

	/**
	 * Default constructor mainly used by Hibernate
	 */
	public User() {

	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", password=****"
				+ ", roleId=" + roleId + "]";
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	

}