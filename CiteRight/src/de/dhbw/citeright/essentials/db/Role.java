package de.dhbw.citeright.essentials.db;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * The representation of the Role Entity
 * 
 * @author Hopf Torsten
 *
 */
@Entity
@Table(name = "Role")
public class Role {
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	Integer id;
	@Column(name = "Name")
	String name;

	// List<Action> actionsAllowedToPerform;
	/**
	 * Creates a Role object with the given information
	 * 
	 * @param id
	 *            the actions id
	 * @param name
	 *            the actions name
	 * @param actionsAllowedToPerform
	 *            the Actions that the Role is allowed to perform
	 */
	public Role(int id, String name, List<Action> actionsAllowedToPerform) {
		this.id = id;
		this.name = name;
		// this.actionsAllowedToPerform = actionsAllowedToPerform;
	}

	/**
	 * Default constructor mainly used by Hibernate
	 */
	public Role() {

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + "]";
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * public List<Action> getActionsAllowedToPerform() { return
	 * actionsAllowedToPerform; } public void
	 * setActionsAllowedToPerform(List<Action> actionsAllowedToPerform) {
	 * this.actionsAllowedToPerform = actionsAllowedToPerform; }
	 */
}
