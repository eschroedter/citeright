package de.dhbw.citeright.essentials.db;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Signleton that creates and returns a Hibernate SessionFactory
 * 
 * @author i13026
 *
 */
public class HibernateUtils {
	private static final SessionFactory sessionFactory;

	static {
		try {
			Configuration configuration = new Configuration()
					.configure(
							HibernateUtils.class
									.getResource("/hibernate.cfg.xml"))
					.addAnnotatedClass(Action.class)
					.addAnnotatedClass(Role.class)
					.addAnnotatedClass(User.class)
					.addAnnotatedClass(Folder.class)
					.addAnnotatedClass(File.class);
			StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
			serviceRegistryBuilder.applySettings(configuration.getProperties());
			ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	/**
	 * Returns the current SessionFactory
	 * 
	 * @return the current SessionFactory instance
	 */
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
