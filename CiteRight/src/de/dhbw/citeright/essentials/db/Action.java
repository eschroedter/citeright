package de.dhbw.citeright.essentials.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Class that represents the Action Entity of the Database
 * 
 * @author Torsten Hopf
 *
 */
@Entity
@Table(name = "Action")
public class Action {
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	private int id;
	@Column(name = "Name")
	private String name;

	/**
	 * Default constructor that is used by Hibernate
	 */
	public Action() {

	}

	/**
	 * 
	 * @return the name of the Action
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            the Name of the Action
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return the Id of the Action
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            the Id of the Action
	 */
	public void setId(int id) {
		this.id = id;
	}
}