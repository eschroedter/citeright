package de.dhbw.citeright.essentials.db;

import java.sql.Timestamp;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Class that represents the File Entity of the Database
 * 
 * @author Torsten Hopf
 *
 */
@Entity
@Table(name = "File")
public class File {
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	Integer id;
	@Column(name = "Filename")
	String filename;
	@Column(name = "Filetype")
	String filetype;
	@Column(name = "data")
	byte[] data;
	@Column(name = "UploadDate")
	Timestamp timestamp;
	@Column(name = "Parent")
	Integer parentId;
	@Column(name = "Reference")
	Integer referenceId = null;

	/**
	 * Creates a File with the given information
	 * 
	 * @param id
	 *            the Id of the file
	 * @param filename
	 *            the name of the file
	 * @param filetype
	 *            the type of the file
	 * @param data
	 *            the data as byte array
	 * @param timestamp
	 *            the creation Date
	 * @param parentId
	 *            the parents Id
	 * @param referenceId
	 *            the reference Id
	 */
	public File(int id, String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId, int referenceId) {
		this.id = id;
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
		this.referenceId = referenceId;
	}

	/**
	 * Creates a File with the given information
	 * 
	 * @param filename
	 *            the name of the file
	 * @param filetype
	 *            the type of the file
	 * @param data
	 *            the data as byte array
	 * @param timestamp
	 *            the creation Date
	 * @param parentId
	 *            the parents Id
	 * @param referenceId
	 *            the reference Id
	 */
	public File(String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId, int referenceId) {
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
		this.referenceId = referenceId;
	}

	/**
	 * Creates a File with the given information
	 * 
	 * @param filename
	 *            the name of the file
	 * @param filetype
	 *            the type of the file
	 * @param data
	 *            the data as byte array
	 * @param timestamp
	 *            the creation Date
	 * @param parentId
	 *            the parents Id
	 */
	public File(String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId) {
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
	}

	/**
	 * Creates a File with the given information
	 * 
	 * @param id
	 *            the Id of the file
	 * @param filename
	 *            the name of the file
	 * @param filetype
	 *            the type of the file
	 * @param data
	 *            the data as byte array
	 * @param timestamp
	 *            the creation Date
	 * @param parentId
	 *            the parents Id
	 */
	public File(int id, String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId) {
		this.id = id;
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
	}

	/**
	 * The default constructor that is mainly used by Hibernate
	 */
	public File() {

	}

	@Override
	public String toString() {
		return "File [id=" + id + ", filename=" + filename + ", filetype="
				+ filetype + ", data=" + Arrays.toString(data) + ", timestamp="
				+ timestamp + ", parentId=" + parentId + ", referenceId="
				+ referenceId + "]";
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(Integer referenceId) {
		this.referenceId = referenceId;
	}

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * @return the filetype
	 */
	public String getFiletype() {
		return filetype;
	}

	/**
	 * @param filetype the filetype to set
	 */
	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}

	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}

	/**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the parentId
	 */
	public Integer getParentId() {
		return parentId;
	}

	/**
	 * @return the referenceId
	 */
	public Integer getReferenceId() {
		return referenceId;
	}



}
