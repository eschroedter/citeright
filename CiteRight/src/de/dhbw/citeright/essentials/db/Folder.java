package de.dhbw.citeright.essentials.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Class that represents the Folder Entity of the Database
 * 
 * @author Torsten Hopf
 *
 */
@Entity
@Table(name = "Folder")
public class Folder {
	@Id
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	@Column(name = "Id", unique = true, nullable = false)
	Integer id;
	@Column(name = "Name")
	String folderName;
	@Column(name = "Token", unique = true)
	String token;
	@Column(name = "Parent")
	Integer parentId;
	@Column(name = "Owner")
	Integer ownerId;

	/**
	 * Creates a Folder object with the given information
	 * 
	 * @param id
	 *            the id of the Folder
	 * @param folderName
	 *            the name of the Folder
	 * @param token
	 *            the Token that identifies the Folder
	 * @param parentId
	 *            the Id of the parent Folder
	 * @param ownerId
	 *            the Id of the User that owns the Folder
	 */
	public Folder(Integer id, String folderName, String token, Integer parentId,
			Integer ownerId) {
		this.id = id;
		this.folderName = folderName;
		this.token = token;
		this.parentId = parentId;
		this.ownerId = ownerId;
	}
	/**
	 * Creates a Folder object with the given information
	 * 
	 * @param folderName
	 *            the name of the Folder
	 * @param token
	 *            the Token that identifies the Folder
	 * @param parentId
	 *            the Id of the parent Folder
	 * @param ownerId
	 *            the Id of the User that owns the Folder
	 */
	public Folder(String folderName, String token, Integer parentId, Integer ownerId) {
		this.folderName = folderName;
		this.token = token;
		this.parentId = parentId;
		this.ownerId = ownerId;
	}
	/**
	 * Default constructor mainly used by Hibernate
	 */
	public Folder() {

	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Folder [id=" + id + ", folderName=" + folderName + ", token="
				+ token + ", parentId=" + parentId + ", ownerId=" + ownerId
				+ "]";
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the folderName
	 */
	public String getFolderName() {
		return folderName;
	}
	/**
	 * @param folderName the folderName to set
	 */
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return the parentId
	 */
	public Integer getParentId() {
		return parentId;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	/**
	 * @return the ownerId
	 */
	public Integer getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	

}