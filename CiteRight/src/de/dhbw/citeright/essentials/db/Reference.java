package de.dhbw.citeright.essentials.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * The representation of the Reference Entity
 * 
 * @author Torsten Hopf
 *
 */
@Entity
@Table(name = "Reference")
public class Reference {
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	Integer id;
	@Column(name = "Name")
	String name;
	@Column(name = "Title")
	String title;
	@Column(name = "Adress")
	String adress;

	/**
	 * Creates a Reference object with the given information
	 * 
	 * @param name
	 *            the name of the Reference
	 * @param title
	 *            the title of the Website
	 * @param adress
	 *            the web address of the site
	 */
	public Reference(String name, String title, String adress) {
		this.name = name;
		this.title = title;
		this.adress = adress;
	}

	/**
	 * Creates a Reference object with the given information
	 * 
	 * @param id
	 *            the Id of the Reference
	 * @param name
	 *            the name of the Reference
	 * @param title
	 *            the title of the Website
	 * @param adress
	 *            the web address of the site
	 */
	public Reference(Integer id, String name, String title, String adress) {
		this.id = id;
		this.name = name;
		this.title = title;
		this.adress = adress;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the adress
	 */
	public String getAdress() {
		return adress;
	}

	/**
	 * @param adress the adress to set
	 */
	public void setAdress(String adress) {
		this.adress = adress;
	}



}