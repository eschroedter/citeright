package de.dhbw.citeright.essentials.db.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import de.dhbw.citeright.essentials.db.Folder;

/**
 * Class that handles the Folder based interactions with the database
 * 
 * @author Enrico Schrödter
 *
 */
public class FolderService extends AbstractService<Folder> {
	/**
	 * Creates a new instance of the FolderService
	 * 
	 * @param sessionFactory
	 *            the sessoin factory that should be used for creating a Session
	 */
	public FolderService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	/**
	 * Returns the Folder that is identified by the given Token
	 * 
	 * @param token
	 *            the Token of the Folder
	 * @return the Folder that is identified by the given Token
	 */
	public Folder getFolderByToken(String token) {
		System.out.println(token);
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session.createQuery("FROM Folder WHERE token = :token");
		query.setParameter("token", token);

		List<Folder> queryResult = query.list();
		transaction.commit();
		session.close();

		if (queryResult.size() > 0) {
			return queryResult.get(0);
		}

		return null;
	}

}
