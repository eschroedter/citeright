package de.dhbw.citeright.essentials.db.service;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * AbstractService provides basic CRUD Functionality
 * @author i13026
 *
 * @param <T>
 */
public abstract class AbstractService<T> {
	protected SessionFactory sessionFactory;
	
	AbstractService(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	/**
	 * Returns the Class of the Generic T
	 * @return the Class of the Generic Type
	 */
	protected Class getGenericType(){
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	/**
	 * Saves a new Element of type T
	 * @param element the element that should be created
	 * @return the Id of the new Element
	 */
	public int create(T element){
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		int id = -1;
		
		id = (int) session.save(element);
		
		
		transaction.commit();
		session.close();
		
		return id;
	}
	
	/**
	 * Returns an Element of Type T, which is identified by id
	 * @param id of the element 
	 * @return the Element
	 */
	public T getById(int id){
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		T user = (T) session.get(getGenericType(), id);
		
		transaction.commit();
		session.close();
		
		return user;
	}
	
	/**
	 * Returns all Elemens of the Table
	 * @return all Elements of the Type
	 */
	public List<T> getAll(){
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		List<T> result = session.createCriteria(getGenericType()).list();
		
		transaction.commit();
		session.close();
		
		return result;
	}
	
	/**
	 * Updates an existing Element of Type T
	 * @param element the element that should be updated
	 * @return true on success
	 */
	public boolean update(T element){
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		session.update(element);
		
		transaction.commit();
		session.close();
		
		return true;
	}
	
	/**
	 * Deletes an Element by id
	 * @param id the Id of the element that should be deleted
	 * @return true on success
	 */
	public boolean delete(int id){
		return delete(getById(id));
	}
	
	/**
	 * Deletes an Element by his object
	 * @param element the element that should be deleted
	 * @return true on sucess
	 */
	public boolean delete(T element){
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		session.delete(element);
		
		transaction.commit();
		session.close();
		
		return true;
	}
}
