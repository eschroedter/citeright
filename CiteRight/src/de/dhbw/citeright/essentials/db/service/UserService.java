package de.dhbw.citeright.essentials.db.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import de.dhbw.citeright.essentials.db.Action;
import de.dhbw.citeright.essentials.db.User;

/**
 * Class that handles the User related interactions with the Database
 * 
 * @author Enrico Schrödter
 *
 */
public class UserService extends AbstractService<User> {
	/**
	 * Creates an UserService instance that used the given SessionFactory for
	 * its Hibernate Sessions
	 * 
	 * @param sessionFactory
	 *            the SessionFactory that should be used
	 */
	public UserService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	/**
	 * Checks if the given login information are correct
	 * 
	 * @param email
	 *            the given email of the user
	 * @param password
	 *            the given password of the user
	 * @return valid login information or not
	 */
	public boolean login(String email, String password) {
		User user = getUserByEmail(email);
		if (user != null) {
			return user.getPassword().equals(password);
		}
		return false;
	}

	/**
	 * Registration for new users Wrapper for create
	 * 
	 * @param user
	 *            the user that wants to get registred
	 * @return id of the new user
	 */
	public int registrate(User user) {
		return create(user);
	}

	/**
	 * Returns the User identified by the email or null
	 * 
	 * @param email
	 *            the email that identifies the user
	 * @return the user if one exists, else null
	 */
	public User getUserByEmail(String email) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session.createQuery("FROM User WHERE email = :email");
		query.setParameter("email", email);

		List<User> queryResult = query.list();
		transaction.commit();
		session.close();

		if (queryResult.size() > 0) {
			return queryResult.get(0);
		}

		return null;
	}

	/**
	 * Checks of the User has the rights to do the Action
	 * 
	 * @param user
	 *            the user that wants to perform an action
	 * @param action
	 *            the action the user wants to perform
	 * @return true if access, else false
	 */
	public boolean hasRightForAction(User user, Action action) {
		return false;
	}

}
