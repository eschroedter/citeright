package de.dhbw.citeright.essentials.db.service;

import org.hibernate.SessionFactory;

import de.dhbw.citeright.essentials.db.File;

/**
 * Class that handles File related interaction with the Database
 * 
 * @author Torsten
 *
 */
public class FileService extends AbstractService<File> {
	/**
	 * Creates an FileService instance
	 * 
	 * @param sessionFactory
	 *            the SessionFactory that is used for Hibernate Sessions
	 */
	public FileService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	
	

}
