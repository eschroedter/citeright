package de.dhbw.citeright.db;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.mock.HibernateUtilsMock;

public class UserServiceTest{

	private static UserService userDAO;

	@BeforeClass
	public static void setUp(){
        SessionFactory sessionFactory = HibernateUtilsMock.getSessionFactory();
        
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        
        System.out.println("CREATE DUMMY DATA: START");
        
        Role role = new Role();
        role.setId(1);
        role.setName("Rolle 1");
        session.save(role);
        
        User user = new User();
        user.setId(1);
        user.setFirstName("Vorame");
        user.setLastName("Nachname");
        user.setEmail("test@test.de");
        user.setRoleId(1);
        user.setPassword("Passwort");
        session.save(user);
        
		User user2 = new User();
        user2.setId(2);
        user2.setFirstName("Vorname");
        user2.setLastName("Nachname");
        user2.setEmail("test2@test.de");
        user2.setRoleId(1);
        user2.setPassword("Passwort");
        session.save(user2);
        
        System.out.println("CREATE DUMMY DATA: END");
        
        transaction.commit();
        session.close();
		
        UserServiceTest.userDAO = new UserService(sessionFactory);
	}
	
	@Test
	public void getNullUserById(){
		User user = userDAO.getById(1000);
		Assert.assertEquals(null, user);
	}
	
	@Test
	public void getExistingUserById(){
		User user = userDAO.getById(1);
		Assert.assertEquals(1, user.getId());
	}
	
	@Ignore
	@Test
	public void getAllUserEmpty(){
		List<User> users = userDAO.getAll();
		
		Assert.assertEquals(0, users.size());
	}
	
	@Ignore
	@Test
	public void getAllUsers(){
		List<User> users = userDAO.getAll();
		
		Assert.assertEquals(2, users.size());
	}
	
	@Test
	public void createUser(){
		User user = new User();
		user.setEmail("mail1@mail.de");
		user.setFirstName("Firstname");
		user.setLastName("Lastname");
		user.setPassword("Passwort");
		user.setRoleId(1);
		
		int id = userDAO.create(user);
		
		Assert.assertEquals(userDAO.getById(id).getEmail(), user.getEmail());
	}
	
	@Ignore
	@Test
	public void createUserWithExistingId(){
		User user = new User();
		user.setEmail("mail@mail.de");
		user.setFirstName("Firstname");
		user.setLastName("Lastname");
		user.setPassword("Passwort");
		user.setRoleId(1);
		user.setId(1);
		
		try{
			userDAO.create(user);
			Assert.fail();
		}catch(Exception e){
			
		}
	}
	
	@Test
	public void updateExistingUser(){
        User user = new User();
        user.setId(1);
        user.setFirstName("Neuer Vorname");
        user.setLastName("Nachname");
        user.setEmail("test@test.de");
        user.setRoleId(1);
        user.setPassword("Passwort");
        
        userDAO.update(user);
		
		Assert.assertEquals("Neuer Vorname", userDAO.getById(1).getFirstName());
	}
	
	@Test
	public void updateNotExisitingUser(){
        User user = new User();
        user.setId(1000);
        user.setFirstName("Neuer Vorname");
        user.setLastName("Nachname");
        user.setEmail("test@test.de");
        user.setRoleId(1);
        user.setPassword("Passwort");
        
        try{
        	userDAO.update(user);
        	Assert.fail();
        }catch(Exception e){
        	
        }  
	}
	
	@Test
	public void deleteExistingUser(){
		User user = new User();
        user.setId(2);
        user.setFirstName("Vorname");
        user.setLastName("Nachname");
        user.setEmail("test2@test.de");
        user.setRoleId(1);
        user.setPassword("Passwort");
        
        userDAO.delete(user);
        
        Assert.assertEquals(null, userDAO.getById(2));
	}
	
	@Test
	public void deleteNotExisitingUser(){
		User user = new User();
        user.setId(1000);
        user.setFirstName("Vorname");
        user.setLastName("Nachname");
        user.setEmail("test2@test.de");
        user.setRoleId(1);
        user.setPassword("Passwort");
        
        try{
        	userDAO.delete(user);
        	Assert.fail();
        }catch(Exception e){
        	
        }
	}
	
	@Test
	public void testValidLogin(){
		boolean result = userDAO.login("test@test.de", "Passwort");
		
		Assert.assertEquals(true, result);
	}
	
	@Test
	public void testInvalidLogin(){
		boolean result = userDAO.login("test@test.de", "Passwort123");
		
		Assert.assertEquals(false, result);
	}
	
	@Test
	public void testNotExistingUserLogin(){
		boolean result = userDAO.login("test@tes", "Passwort");
		
		Assert.assertEquals(false, result);
	}
	
	@Test
	public void testGetExistingUserByEmail(){
		User user = userDAO.getUserByEmail("test@test.de");
		
		Assert.assertEquals(1, user.getId());
	}
	
	@Test
	public void testGetNotExistingUserByEmail(){
		User user = userDAO.getUserByEmail("test@test.de123123");
		
		Assert.assertEquals(null, user);
	}
}
