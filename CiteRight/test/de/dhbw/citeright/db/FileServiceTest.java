package de.dhbw.citeright.db;

import java.sql.Timestamp;
import java.util.Date;

import junit.framework.Assert;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.BeforeClass;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.File;
import de.dhbw.citeright.essentials.db.Folder;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.FileService;
import de.dhbw.citeright.mock.HibernateUtilsMock;

public class FileServiceTest {
	private static FileService fileServiceDAO;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SessionFactory sessionFactory = HibernateUtilsMock.getSessionFactory();

		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		System.out.println("CREATE DUMMY DATA: START");

		Role role = new Role();
		role.setId(1);
		role.setName("Rolle1");

		session.save(role);
		System.out.println("CREATED " + role.toString());

		User owner = new User();
		owner.setId(1);
		owner.setFirstName("Max");
		owner.setLastName("Mustermann");
		owner.setEmail("Kai.Holzer@mustermail.com");
		owner.setRoleId(role.getId());
		owner.setPassword("sicheres_passwort");
		session.save(owner);
		System.out.println("CREATED " + owner.toString());

		Folder folder1 = new Folder(1, "MusterROOTOrdner", "ASDF1234", null, 1);
		session.save(folder1);
		System.out.println("CREATED " + folder1.toString());

		File file1 = new File();
		file1.setFilename("Fänger im Roggen");
		file1.setFiletype("text/plain");
		file1.setId(1);
		file1.setParentId(folder1.getId());
		file1.setTimestamp(new Timestamp(new Date().getTime()));
		file1.setReferenceId(null);
		file1.setData(new byte[] { 22, 44, 55, 66, 77, 88, 99, 111, 22, 44 });

		System.out.println("CREATED " + file1.toString());

		transaction.commit();
		System.out.println("COMMITED");
		session.close();

		fileServiceDAO = new FileService(sessionFactory);
		System.out.println("CREATE DUMMY DATA: END");
	}

	@Test
	public void testCreate() {
		File toCreate = new File(234, "Kai im Wald", "Text", new byte[] { 0, 2,
				3, 4 }, new Timestamp(new Date().getTime()), 1);
		Integer id = fileServiceDAO.create(toCreate);
		Assert.assertEquals("Kai im Wald", fileServiceDAO.getById(id)
				.getFilename());
	}

	@Test
	public void testGetById() {
		Assert.assertNotNull(fileServiceDAO.getById(1));
	}

	@Test
	public void testGetAll() {
		Assert.assertTrue(fileServiceDAO.getAll().size() > 0);
	}

	@Test
	public void testUpdate() {
		File toUpdate = new File(88, "Fröhlich einen Holzern", "Text",
				new byte[] { 0, 2, 3, 4 }, new Timestamp(new Date().getTime()),
				1);
		Integer id = fileServiceDAO.create(toUpdate);
		toUpdate.setFilename("Fröhlich einen Weggeholzert");
		fileServiceDAO.update(toUpdate);
		
		Assert.assertEquals("Fröhlich einen Weggeholzert", fileServiceDAO
				.getById(id).getFilename());

	}

	@Test
	public void testDeleteInt() {
		File toDelete = new File(66, "Heute schon den Kai angerufen?", "Text",
				new byte[] { 0, 2, 3, 4 }, new Timestamp(new Date().getTime()),
				1);
		Integer id = fileServiceDAO.create(toDelete);
		
		fileServiceDAO.delete(id);
		Assert.assertNull(fileServiceDAO.getById(id));
		
		
		
	}

	@Test
	public void testDeleteT() {
		File toDelete = new File(77, "Mein Boot am Kai", "Text",
				new byte[] { 0, 2, 3, 4 }, new Timestamp(new Date().getTime()),
				1);
		fileServiceDAO.create(toDelete);
		fileServiceDAO.delete(toDelete);
		
		Assert.assertNull(fileServiceDAO.getById(77));
	}

}
