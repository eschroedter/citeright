package de.dhbw.citeright.db;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.Assert;
import org.junit.Test; 

import de.dhbw.citeright.essentials.db.User;
@SuppressWarnings("deprecation")
public class HibernateConfigTest {

	@Test
	public void test() {
		SessionFactory factory;
		factory = new AnnotationConfiguration().
                configure().
                //addPackage("com.xyz") //add package if used.
                addAnnotatedClass(User.class).
                buildSessionFactory();
		
		Session session = factory.openSession();
		List<User> users  = session.createQuery("From User").list();
		Assert.assertFalse(users.isEmpty());
		session.close();
		factory.close();
	}

}
