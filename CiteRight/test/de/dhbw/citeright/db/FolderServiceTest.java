package de.dhbw.citeright.db;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.Folder;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.FolderService;
import de.dhbw.citeright.mock.HibernateUtilsMock;

public class FolderServiceTest {

	private static FolderService FolderServiceDAO;

	@BeforeClass
	public static void setUp() throws Exception {
		SessionFactory sessionFactory = HibernateUtilsMock.getSessionFactory();

		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		System.out.println("CREATE DUMMY DATA: START");

		Role role = new Role();
		role.setId(1);
		role.setName("Rolle1");

		session.save(role);
		System.out.println("CREATED " + role.toString());

		User owner = new User();
		owner.setId(1);
		owner.setFirstName("Max");
		owner.setLastName("Mustermann");
		owner.setEmail("Kai.Holzer@mustermail.com");
		owner.setRoleId(role.getId());
		owner.setPassword("sicheres_passwort");
		session.save(owner);
		System.out.println("CREATED " + owner.toString());

		Folder folder1 = new Folder(1,"MusterROOTOrdner", "ASDF1234", null, 1);
		Folder folder2 = new Folder(2, "SecondMusterOrdner", "1234QWERT", 1, 1);
		session.save(folder1);
		session.save(folder2);
		System.out.println("CREATED " + folder1.toString());
		System.out.println("CREATED " + folder2.toString());

		transaction.commit();
		System.out.println("COMMITED");
		session.close();

		FolderServiceTest.FolderServiceDAO = new FolderService(sessionFactory);

		System.out.println("CREATE DUMMY DATA: END");
	}

	@Test
	public void testGetFolderByToken() {
		FolderServiceDAO.create(new Folder("Test","TEST123",1,1));
		Folder folder = FolderServiceDAO.getFolderByToken("TEST123");
		Assert.assertNotNull(folder);
		Assert.assertEquals("MusterROOTOrdner", folder.getFolderName());
	}

	@Test
	public void testCreate() {
		System.out.println("START CREATE");
		Folder folder = new Folder(255, "Kais Ordner der Sünden", "M1LF2", 1, 1);
		FolderServiceDAO.create(folder);
		Assert.assertEquals(folder.getFolderName(),
				FolderServiceDAO.getById(folder.getId()).getFolderName());
		System.out.println("END CREATE");
	}

	@Test
	public void testGetById() {
		System.out.println("START GET BY ID");
		Folder folder = FolderServiceDAO.getById(1);
		Assert.assertEquals("MusterROOTOrdner", folder.getFolderName());
		System.out.println("END GET BY ID");
	}

	@Test
	public void testGetAll() {
		System.out.println("START GET ALL");
		List<Folder> folders = FolderServiceDAO.getAll();
		Assert.assertTrue(folders.size() > 1 );
		System.out.println("END GET ALL");
	}

	@Test
	public void testUpdate() {
		System.out.println("START UPDATE");
		Folder folder = FolderServiceDAO.getById(1);
		folder.setToken("NEWTOKEN");
		FolderServiceDAO.update(folder);
		Assert.assertEquals("NEWTOKEN",folder.getToken());
		System.out.println("END UPDATE");
		}

	@Test
	public void testDeleteInt() {
		System.out.println("START DELETE ID");
		Folder folderToDel = new Folder("Badeparadies Schwarzwald","SONNE3",1,1);
		FolderServiceDAO.create(folderToDel);
		FolderServiceDAO.delete(folderToDel.getId());
		Assert.assertNull(FolderServiceDAO.getById(folderToDel.getId()));
		System.out.println("END DELETE ID");
		
	}

	@Test
	public void testDeleteT() {
		System.out.println("START DELETE OBJECT");
		Folder folder = new Folder("Was soll das?","WASDENN",1,1);
		Integer id = FolderServiceDAO.create(folder);
		FolderServiceDAO.delete(folder);
		Assert.assertNull(FolderServiceDAO.getById(id));
		System.out.println("END DELETE OBJECT");
	}

}
