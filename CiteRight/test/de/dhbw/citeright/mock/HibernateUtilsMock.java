package de.dhbw.citeright.mock;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import de.dhbw.citeright.essentials.db.Action;
import de.dhbw.citeright.essentials.db.File;
import de.dhbw.citeright.essentials.db.Folder;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.User;

public class HibernateUtilsMock {
	private static final SessionFactory sessionFactory;

	static {
		try {
			Configuration configuration = new Configuration()
					.configure(
							HibernateUtilsMock.class
									.getResource("/hibernate.test.cfg.xml"))
					.addAnnotatedClass(Action.class)
					.addAnnotatedClass(Role.class)
					.addAnnotatedClass(User.class)
					.addAnnotatedClass(File.class)
					.addAnnotatedClass(Folder.class)
					;
			StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
			serviceRegistryBuilder.applySettings(configuration.getProperties());
			ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
