package saveHTML;

//package saveHTML;
//
//import akka.actor.Props;
//import akka.actor.UntypedActor;
//
///**
// * Actor-Class for starting the saving of an HTML file
// * 
// * @author Antonio
// * @since 29.04.2015
// */
//public class SaveHtmlActor extends UntypedActor {
//
//	/**
//	 * 
//	 * @return a reference to the SaveHtmlActor
//	 */
//	public Props getProps() {
//		return Props.create(SaveHtmlActor.class);
//	}
//
//	/**
//	 * Handles the receive of messages and their editing
//	 * 
//	 * @param object
//	 */
//	@Override
//	public void onReceive(Object message) throws Exception {
//		if (message instanceof SaveHtmlMessage) {
//			
//			SaveHtmlMessage saveHtmlMessage = (SaveHtmlMessage) message;
//			String urlString = saveHtmlMessage.getURLasString();
//			
//			SaveHtmlAsFile saver = new SaveHtmlAsFile();
//			saver.saveHTML(urlString);
//			// TODO: is it necessary to start the next section?
//
//		} else {
//			unhandled(message);
//		}
//
//	}
//
// }
