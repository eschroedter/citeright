package saveHTML

import akka.actor._
import de.dhbw.citeright.server.search.SearchActor.DocumentSearchResultsMessage
object SaveHtmlActor {
  def props(compareActor: ActorRef): Props = Props(new SaveHtmlActor(compareActor))

  case class ConvertSearchResultsMessage()
}
class SaveHtmlActor(CompareActor: ActorRef) extends Actor {
  import SaveHtmlActor._
  def receive = {

    case DocumentSearchResultsMessage(sectionResults) => {
      sectionResults.foreach { sectionResult =>
        {
          sectionResult.searchResults.foreach { searchResult => new SaveHtmlAsFile saveHTML searchResult.getUrl }
        }
        CompareActor ! new ConvertSearchResultsMessage()
      }
    }
    case _ =>
  }
}