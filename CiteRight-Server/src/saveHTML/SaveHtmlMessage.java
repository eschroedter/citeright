package saveHTML;

/**
 * Dataclass for URLs (as String) to save the HTML code in files
 * 
 * @author Antonio
 * @since 29.04.2015
 */
public class SaveHtmlMessage {
	private final String urlString;

	/**
	 * Constructor
	 * 
	 * @param phrases
	 */
	public SaveHtmlMessage(String urlString) {
		this.urlString = urlString;
	}

	/**
	 * 
	 * @return <String> URL as String
	 */
	public String getURLasString() {
		return urlString;
	}
}
