package saveHTML;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Class to Save a HTML (reachable over an URL) on a HTML file on the DB-Server
 * 
 * @author Antonio
 * @since 29.04.2015
 */
public class SaveHtmlAsFile {

	/**
	 * Check if the website is available
	 * From stackoverflow
	 * http://stackoverflow.com/questions/3584210/preferred-java-way-to-ping-a-http-url-for-availability
	 * @param urlString
	 * @param timeout
	 * @return Boolean true = site is reachable
	 */
	public boolean ping(String url, int timeout) {
	    // Otherwise an exception may be thrown on invalid SSL certificates:
		url = url.replaceFirst("^https", "http");

	    try {
	        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
	        connection.setConnectTimeout(timeout);
	        connection.setReadTimeout(timeout);
	        connection.setRequestMethod("HEAD");
	        int responseCode = connection.getResponseCode();
	        return (200 <= responseCode && responseCode <= 399);
	    } catch (IOException exception) {
	        return false;
	    }
	}
	
	/**
	 * Call the website and write its Code into a HTML file on the DB-Server
	 * 
	 * @param urlString
	 *            URL of the website as String
	 */
	public void saveHTML(String urlString) {
		// check if site is reachable
		boolean reachable = ping(urlString, 10000);
		
		// if site is not reachable do not create a file
		if(reachable)
		{

			FileOutputStream fileOutStream = null;
			URL url;

			try {
				url = new URL(urlString);
				// open stream to the source URL
				ReadableByteChannel rbc = Channels.newChannel(url.openStream());
				// TODO create file on DB-Server
				fileOutStream = new FileOutputStream("out.html");
				// copy file from the source
				fileOutStream.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			} catch (MalformedURLException mue) {
				mue.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} finally {
				try { // Close Output Stream
					if (fileOutStream != null) {
						fileOutStream.close();
					}
				} catch (IOException e) {
					// Stream has not been started
				}
			}
		}
	}

}
