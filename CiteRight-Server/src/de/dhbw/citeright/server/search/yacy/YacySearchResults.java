package de.dhbw.citeright.server.search.yacy;

import com.google.gson.annotations.SerializedName;

/**
 * Dataclass for searchresults
 * 
 * @author Florian Knecht
 * @since 29.04.2015
 *
 */
public class YacySearchResults {

	@SerializedName("items")
	public Item[] items;

	/**
	 * Representation of an result
	 * 
	 * @author Florian Knecht
	 * @since 29.04.2015
	 */
	public static class Item {
		@SerializedName("title")
		public String title;
		@SerializedName("description")
		public String description;
		@SerializedName("link")
		public String link;

	}
}
