package de.dhbw.citeright.server.search;

//package de.dhbw.citeright.server.search;
//
//import java.util.List;
//
///**
// * Dataclass for all searchResultMessages of an document
// * 
// * @author Moritz Ragg
// * @since 29.04.2015
// */
//public class DocumentSearchResultsMessage {
//	private final List<SearchResultsMessage> sectionResults;
//
//	/**
//	 * Constructor
//	 * 
//	 * @param sectionResults
//	 */
//	public DocumentSearchResultsMessage(
//			List<SearchResultsMessage> sectionResults) {
//		this.sectionResults = sectionResults;
//	}
//
//	/**
//	 * 
//	 * @return List<SearchResultsMessage> searchResults by sections of the
//	 *         document
//	 */
//	public List<SearchResultsMessage> getSectionResults() {
//		return sectionResults;
//	}
//
// }
