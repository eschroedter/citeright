package de.dhbw.citeright.server.search;

import java.io.IOException;
import java.util.List;

/**
 * 
 * Interface for Search-Classes
 * 
 * @author Moritz Ragg
 * @since 29.04.2015
 */
public interface SearchEngine {

	/**
	 * 
	 * @param phrase
	 * @return list of SearchResults of search with search engine
	 * @throws IOException
	 */
	public List<SearchResult> search(String phrase) throws IOException;

	/**
	 * 
	 * @param phrase
	 * @param limit
	 * @return list of SearchResults of search with search engine
	 * @throws IOException
	 */
	public List<SearchResult> search(String phrase, int limit)
			throws IOException;
}
