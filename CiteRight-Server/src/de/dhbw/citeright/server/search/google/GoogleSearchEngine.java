package de.dhbw.citeright.server.search.google;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;

import de.dhbw.citeright.server.search.SearchEngine;
import de.dhbw.citeright.server.search.SearchResult;
import de.dhbw.citeright.server.search.google.GoogleSearchResults.Result;

/**
 * Class for searching with google searchEngine
 * 
 * @author Moritz Ragg
 * @since 29.04.2015
 */
public class GoogleSearchEngine implements SearchEngine {
	private String address = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0";
	private String charset = "UTF-8";

	/**
	 * Searches via bing SearchEngine for results for the phrase
	 * 
	 * @param String
	 *            search phrase
	 * @return list of SearchResults
	 */
	@Override
	public List<SearchResult> search(String phrase, int limit)
			throws IOException {
		GoogleSearchResults results;
		List<SearchResult> resultList = new LinkedList<SearchResult>();
		int iterate;

		try {
			for (int i = 0; i < limit; i += 4) {

				URL url = new URL(this.createURL(i)
						+ URLEncoder.encode(phrase + " filetype:html", charset));
				Reader reader = new InputStreamReader(url.openStream(), charset);
				results = new Gson()
						.fromJson(reader, GoogleSearchResults.class);

				if (limit - i < 4) {
					iterate = limit - i;
				} else {
					iterate = 4;
				}
				if (results.getResponseData().getResults().size() < iterate) {
					iterate = results.getResponseData().getResults().size();
				}

				for (int j = 0; j < iterate; j++) {
					String resultTitle = results.getResponseData().getResults()
							.get(j).getTitle();
					String resultUrl = results.getResponseData().getResults()
							.get(j).getUrl();
					resultList.add(new SearchResult(resultTitle, resultUrl));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return resultList;
	}

	/**
	 * Searches via bing SearchEngine for results for the phrase
	 * 
	 * @param String
	 *            search phrase
	 * @param int limit of results
	 * @return list of SearchResults
	 */
	@Override
	public List<SearchResult> search(String phrase) throws IOException {
		GoogleSearchResults results;
		List<SearchResult> resultList = new LinkedList<SearchResult>();

		try {
			URL url = new URL(this.createURL(0)
					+ URLEncoder.encode(phrase, charset));
			Reader reader = new InputStreamReader(url.openStream(), charset);
			results = new Gson().fromJson(reader, GoogleSearchResults.class);

			for (Result result : results.getResponseData().getResults()) {
				resultList.add(new SearchResult(result.getTitle(), result
						.getUrl()));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return resultList;
	}

	/**
	 * 
	 * @param offset
	 * @return URL
	 */
	public String createURL(int offset) {
		return address + "&start=" + offset + "&q=";
	}

}
