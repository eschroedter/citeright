package de.dhbw.citeright.server.search.google;

import java.util.List;

/**
 * Dataclass for searchresults
 * 
 * @author Moritz Ragg
 * @since 29.04.2015
 *
 */
class GoogleSearchResults {

	private ResponseData responseData;

	public ResponseData getResponseData() {
		return responseData;
	}

	public void setResponseData(ResponseData responseData) {
		this.responseData = responseData;
	}

	public String toString() {
		return "ResponseData[" + responseData + "]";
	}

	/**
	 * Holds the content of a result
	 * 
	 * @author Moritz Ragg
	 * @since 29.04.2015
	 */
	static class ResponseData {
		private List<Result> results;

		public List<Result> getResults() {
			return results;
		}

		public void setResults(List<Result> results) {
			this.results = results;
		}

		public String toString() {
			return "Results[" + results + "]";
		}
	}

	/**
	 * Representation of an result
	 * 
	 * @author Moritz Ragg
	 * @since 29.04.2015
	 */
	static class Result {
		private String url;
		private String title;

		public String getUrl() {
			return url;
		}

		public String getTitle() {
			return title;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String toString() {
			return "Result[url:" + url + ",title:" + title + "]";
		}
	}
}
