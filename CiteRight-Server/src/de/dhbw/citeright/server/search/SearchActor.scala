package de.dhbw.citeright.server.search;

import akka.actor._
import extract.ExtractActor.SearchPhrasesMessage
import de.dhbw.citeright.server.search.google.GoogleSearchEngine
import de.dhbw.citeright.server.search.bing.BingSearchEngine
import de.dhbw.citeright.server.search.SearchResult
import scala.collection.JavaConversions._
import extract.Section
import scala.collection.JavaConversions
import scala.collection.mutable.Buffer
/**
 * Provides Props and the messages this Actor sends
 */
object SearchActor {
  def props(SaveHtmlActor: ActorRef): Props = Props(new SearchActor(SaveHtmlActor))

  case class SectionResults(section: Section, searchResults: Buffer[SearchResult])
  case class DocumentSearchResultsMessage(sectionResults: Buffer[SectionResults])
}
/**
 * Uses the Google- and Bing-Api to search for the given terms
 * on the internet
 */
class SearchActor(SaveActor: ActorRef) extends Actor {
  import SearchActor._
  def receive = {
    case SearchPhrasesMessage(sections) => {
      val googleEngine = new GoogleSearchEngine()
      val bingEngine = new BingSearchEngine()

      //Map every section from sections to some new SectionResults. These contain the current Section        
      //and the search results for the section's headwords
      //This generates a list of SectionResults
      val sectionResults = sections.map { section =>
        //TODO: Validating, if sectionText is null
        val bingResults = bingEngine.search(section.getHeadwords, 5)
        val googleResults = googleEngine.search(section.getHeadwords, 5)
        val results = (googleResults ++ bingResults).toBuffer
        SectionResults(section, results)
      }

      SaveActor ! DocumentSearchResultsMessage(sectionResults)
    }
  }
}