package de.dhbw.citeright.server.search;

//import java.util.ArrayList;
//import java.util.List;
//
//import de.dhbw.citeright.server.search.bing.BingSearchEngine;
//import de.dhbw.citeright.server.search.google.GoogleSearchEngine;
//import de.dhbw.citeright.server.extract.SearchPhrasesMessageReference;
//import de.dhbw.citeright.server.extract.Section;
//import akka.actor.ActorRef;
//import akka.actor.Props;
//import akka.actor.UntypedActor;
//import akka.dispatch.Futures;
//import akka.dispatch.OnSuccess;
//
///**
// * Actor-Class for starting searches
// * 
// * @author Moritz Ragg
// * @since 29.04.2015
// */
//public class SearchActorReference extends UntypedActor {
//	private ActorRef next;
//
//	/**
//	 * Constructor
//	 * 
//	 * @param next
//	 */
//	public SearchActorReference(ActorRef next) {
//		this.next = next;
//	}
//
//	/**
//	 * 
//	 * @return a reference to the SearchActor
//	 */
//	public Props getProps() {
//		return Props.create(SearchActor.class);
//	}
//
//	/**
//	 * Handles the receive of messages and their editing
//	 * 
//	 * @param object
//	 * @return answer message
//	 */
//	@Override
//	public void onReceive(Object message) throws Exception {
//		if (message instanceof SearchPhrasesMessageReference) {
//
//			SearchPhrasesMessageReference sectionsMessage = (SearchPhrasesMessageReference) message;
//
//			SearchEngine googleEngine = new GoogleSearchEngine();
//			SearchEngine bingEngine = new BingSearchEngine();
//
//			List<SearchResultsMessage> sectionResults = new ArrayList<SearchResultsMessage>();
//
//			for (Section section : sectionsMessage.getPhrases()) {
//				List<SearchResult> results = new ArrayList<SearchResult>();
//				String headwords = section.getHeadwords();
//
//				Future<List<SearchResult>> googleSearchResults = Futures
//						.future(new Searcher(googleEngine, headwords, 5),
//								system.dispatcher());
//				Future<List<SearchResult>> bingSearchResults = Futures.future(
//						new Searcher(bingEngine, headwords, 5),
//						system.dispatcher());
//
//				googleSearchResults.onSuccess(
//						new OnSuccess<List<SearchResult>>() {
//							@Override
//							public onSucces(List<SearchResult> list) {
//								results.addAll(list);
//							}
//						}, system.dispatcher());
//
//				bingSearchResults.onSuccess(
//						new OnSuccess<List<SearchResult>>() {
//							@Override
//							public onSucces(List<SearchResult> list) {
//								results.addAll(list);
//							}
//						}, system.dispatcher());
//				// TODO: add yacy
//				sectionResults.add(new SearchResultsMessage(section, results));
//			}
//
//			DocumentSearchResultsMessage results = new DocumentSearchResultsMessage(
//					sectionResults);
//			next.tell(results, getSelf());
//
//		} else {
//			unhandled(message);
//		}
//
//	}
// }
