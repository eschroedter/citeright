package de.dhbw.citeright.server.search.bing;

/**
 * Dataclass for searchresults
 * 
 * @author Florian Knecht
 * @since 29.04.2015
 *
 */
public class BingSearchResults {

	public ResultsContent d;

	/**
	 * Holds the content of a result
	 * 
	 * @author Florian Knecht
	 * @since 29.04.2015
	 */
	public static class ResultsContent {
		public Result[] results;
		public String __next;
	}

	/**
	 * Representation of an result
	 * 
	 * @author Florian Knecht
	 * @since 29.04.2015
	 */
	public static class Result {
		public String ID;
		public String Title;
		public String Description;
		public String DisplayUrl;
		public String Url;
		public Metadata __metadata;

	}

	/**
	 * Dataclass for metadata
	 * 
	 * @author Florian Knecht
	 * @since 29.04.2015
	 */
	public static class Metadata {
		public String uri;
		public String type;
	}

}
