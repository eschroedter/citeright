package extract

import akka.actor._
import scala.concurrent.Await
import scala.concurrent.Future
import akka.pattern.ask
import akka.util.Timeout
import akka.dispatch.Futures
import scala.concurrent.duration._
import convert.ConvertActor.ConvertedDocumentMessage
import scala.collection.JavaConversions._
import convert.Document
import scala.collection.mutable.Buffer

/**
 * Provides props and other static members
 *
 */
object ExtractActor {
  def props(searchActor: ActorRef): Props = Props(new ExtractActor(searchActor))
  case class SearchPhrasesMessage(phrases: Buffer[Section])
}
/**
 * removes nonrelevant words of a ConvertedDocumentMessage
 *  extracts headwords of the sections
 *  creates a SearchPhrasesMessage and sends it to a SearchActor
 */
class ExtractActor(searchActor: ActorRef) extends Actor {
  import ExtractActor._
  def receive = {
    case ConvertedDocumentMessage(document) => {
      import context.dispatcher
      //TODO get from Database
      val nonRelevantWords = List("der", "die", "das")
      val sections = document.getAllSections
      val repliesFiltered = sections.map {
        section => Future { new RemoveNonRelevantWords(section, nonRelevantWords).call() }
      }
      Future.sequence(repliesFiltered).onSuccess {
        case filteredSections =>
          val repliesRelevant = filteredSections.map {
            case filteredSection => Future {
              filteredSection.setHeadwords(
                WordFrequency.getWordsInDescendingFreqOrder(filteredSection.getSectionText))
              filteredSection
            }
          }
          Future.sequence(repliesRelevant).onSuccess {
            case extractedSections => searchActor ! new SearchPhrasesMessage(extractedSections)
          }

      }
    }
    case _ =>
  }
}