package extract;

import java.util.List;

/**
 * Dataclass which represents a section of a document
 * 
 * @author Moritz Ragg
 * @since 29.04.2015
 */
public class Section {
	private String sectionText;
	private String headWords;
	private String document;
	private final int orderingNumber;

	/**
	 * 
	 * @param sectionText
	 *            content of a section
	 * @param headWords
	 *            headwords of the content
	 * @param document
	 *            path of the document where the section is written down
	 * @param orderingNumber
	 *            number which of the sections of a document this is
	 */
	public Section(String sectionText, String headWords, String document,
			int orderingNumber) {
		this.sectionText = sectionText;
		this.headWords = headWords;
		this.document = document;
		this.orderingNumber = orderingNumber;
	}

	/**
	 * sets the section text
	 * 
	 * @param sectionText
	 */
	public void setSectionText(String sectionText) {
		this.sectionText = sectionText;
	}

	/**
	 * sets new headwords
	 * 
	 * @param headwords
	 */
	public void setHeadwords(List<String> headwords) {
		for (String word : headwords) {
			this.headWords = this.headWords.concat(word + "+");
		}
	}

	/**
	 * 
	 * @return String section content
	 */
	public String getSectionText() {
		return sectionText;
	}

	/**
	 * 
	 * @return section headwords
	 */
	public String getHeadwords() {
		return headWords;
	}

	/**
	 * 
	 * @return document, where the section is written down
	 */
	public String getDocument() {
		return document;
	}

	/**
	 * 
	 * @return section ordering number
	 */
	public int getOrderingNumber() {
		return orderingNumber;
	}

}
