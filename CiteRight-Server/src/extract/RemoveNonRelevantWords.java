package extract;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Callable which removes a words from Section
 * 
 * @author Moritz Ragg
 * @since 29.04.2015
 */
public class RemoveNonRelevantWords implements Callable<Section> {
	private Section section;
	private List<String> toRemove;

	/**
	 * Constructor
	 * 
	 * @param section
	 * @param toRemove
	 */
	public RemoveNonRelevantWords(Section section, List<String> toRemove) {
		this.section = section;
		this.toRemove = toRemove;
	}

	/**
	 * @return section without removed word
	 */
	@Override
	public Section call() throws Exception {
		String content = section.getSectionText();
		for (String word : toRemove) {
			content = content.replace(" " + word + " ", " ")
					.replace(" " + word + ",", ",")
					.replace(" " + word + ".", ".");
		}
		section.setSectionText(content);
		return section;
	}
}
