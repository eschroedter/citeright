package extract;

//import convert.ConvertedDocumentMessage;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//import akka.actor.ActorRef;
//import akka.actor.UntypedActor;
//import akka.dispatch.Futures;
//
///**
// * Removes nonrelevant words an extract headwords
// * 
// * @author Moritz Ragg
// * @since 29.04.2015
// */
//public class ExtractActorReference extends UntypedActor {
//	private ActorRef next;
//
//	public ExtractActorReference(ActorRef next) {
//		this.next = next;
//	}
//
//	/**
//	 * removes nonrelevant words of a ConvertedDocumentMessage extracts
//	 * headwords of the sections creates a SearchPhrasesMessage and send it to
//	 * an SearchActor
//	 * 
//	 * @param object
//	 */
//	@Override
//	public void onReceive(Object message) throws Exception {
//		if (message instanceof ConvertedDocumentMessage) {
//			ConvertedDocumentMessage documentMessage = (ConvertedDocumentMessage) message;
//			List<String> nonRelevantWords = new ArrayList<String>();
//			List<Section> filtered = new ArrayList<Section>();
//
//			// TODO: Remove the following dummies
//			nonRelevantWords.add("der");
//			nonRelevantWords.add("die");
//			nonRelevantWords.add("das");
//
//			// TODO: database connection
//			switch (documentMessage.getDocument().getLanguage()) {
//			case GERMAN:
//				// nonRelevantWords = database.getNonRlevantWords(GERMAN);
//				break;
//			case ENGLISH:
//				// nonRelevantWords = database.getNonRlevantWords(ENGLISH);
//				break;
//			case FRENCH:
//				// nonRelevantWords = database.getNonRlevantWords(FRENCH);
//				break;
//			default:
//				// nonRelevantWords = database.getNonRlevantWords(GERMAN);
//				break;
//			}
//
//			for (Section section : documentMessage.getDocument()
//					.getAllSections()) {
//				Future<Section> reply = Futures.future(
//						new RemoveNonRelevantWords(section, nonRelevantWords),
//						system.dispatcher());
//				reply.onSuccess(new OnSuccess<Section>() {
//					@Override
//					public void onSuccess(Section reply) {
//						reply.setHeadWords(WordFrequency
//								.getWordsInDescendingFreqOrder(reply
//										.getSectionText()));
//						filtered.add(reply);
//					}
//				}, system.dispatcher());
//			}
//
//			SearchPhrasesMessage searchPhrasesMessage = new SearchPhrasesMessage(
//					filtered);
//			next.tell(searchPhrasesMessage, getSelf());
//
//		} else {
//			unhandled(message);
//		}
//
//	}
//
// }
