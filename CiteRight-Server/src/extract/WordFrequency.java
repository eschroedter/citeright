package extract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to get a list of words out of a String the words are ordered descending
 * after the frequency
 * 
 * @author Antonio
 * @since 29.04.2015
 */
public class WordFrequency {

	/**
	 * Separate a String into words and orders them descending by frequency
	 * appearance
	 * 
	 * @param textOfSection
	 * @return List of Strings as sorted list descending
	 */
	public static List<String> getWordsInDescendingFreqOrder(
			String textOfSection) {
		String[] singleWords = textOfSection.split(" ");

		Map<String, Integer> map = new HashMap<>();
		for (int i = 0; i < singleWords.length; i++) {
			// change all words to lower case for case insensitivity
			singleWords[i] = singleWords[i].toLowerCase();
			if (map.containsKey(singleWords[i])) {
				map.put(singleWords[i],
						new Integer(map.get(singleWords[i]) + 1));
			} else { // word is not in the map
				// put word into map with frequency 1
				map.put(singleWords[i], new Integer(1));
			}
		}

		List<String> sortedList = new ArrayList<String>();
		sortedList = orderInDescendingFreqSequence(map);

		return sortedList;
	}

	/**
	 * Sorts the map in descending order of the frequency into a list
	 * 
	 * @param wordCount
	 * @return List of Strings as sorted list descending
	 */
	private static List<String> orderInDescendingFreqSequence(
			Map<String, Integer> wordCount) {

		List<Map.Entry<String, Integer>> list = new ArrayList<>(
				wordCount.entrySet());

		// Sort list by integer values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> term1,
					Map.Entry<String, Integer> term2) {
				return (term2.getValue()).compareTo(term1.getValue());
			}
		});

		// Put sorted words into a list
		List<String> sortedList = new ArrayList<String>();
		for (Map.Entry<String, Integer> entry : list) {
			sortedList.add(entry.getKey());
		}

		return sortedList;
	}
}
