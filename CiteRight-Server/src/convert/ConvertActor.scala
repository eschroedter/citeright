package convert

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import main.ServerActor.ConvertThisMessage
object ConvertActor {
  def props(extract: ActorRef): Props = Props(new ConvertActor(extract))
  case class ConvertedDocumentMessage(document: Document)
}
class ConvertActor(extract: ActorRef) extends Actor {
  import ConvertActor._
  def receive = {
    case ConvertThisMessage(file) => {
      val document = new FileParser().toDocument(file, 10)
      extract ! new ConvertedDocumentMessage(document)
    }
    case _ =>
  }
}