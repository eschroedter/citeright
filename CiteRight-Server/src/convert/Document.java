package convert;

import java.util.List;

import extract.Section;

/**
 * Dataclass for holding all sections of a document
 * 
 * @author Moritz Ragg
 * @since 29.04.2015
 */
public class Document {

	private List<Section> sections;
	private Language language;

	public Document() {
		// Standard constructor
	}

	/**
	 * Constructor
	 * 
	 * @param sections
	 */
	public Document(List<Section> sections) {
		this.sections = sections;
	}

	/**
	 * Adds a section
	 * 
	 * @param section
	 */
	public void addSection(Section section) {
		this.sections.add(section);
	}

	/**
	 * Sets the language of an document
	 * 
	 * @param language
	 */
	public void setLanguage(Language language) {
		this.language = language;
	}

	/**
	 * Returns the section choosen by id
	 * 
	 * @param id
	 * @return section of id
	 */
	public Section getSectionByID(int id) {
		return sections.get(id);
	}

	/**
	 * 
	 * @return sections
	 */
	public List<Section> getAllSections() {
		return this.sections;
	}

	/**
	 * 
	 * @return language of the document
	 */
	public Language getLanguage() {
		return this.language;
	}
}
