package convert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import org.apache.tika.detect.AutoDetectReader;
import org.apache.tika.exception.TikaException;

import extract.Section;

public class FileParser {

	/**
	 * Transforms the given File to a Document the application can work with
	 * Splits the text of the file into groups of 5 lines. Those are then
	 * converted to the Sections to be stored in the document.
	 * 
	 * @param file
	 *            A file in any of the formats apache tika supports. See: <a
	 *            href="https://tika.apache.org/1.8/formats.html">https://tika.
	 *            apache.org/1.8/formats.html</a>
	 * @param sectionLength
	 *            How long each of the Sections should be
	 * @return A Document containing the contents of file in plain text,
	 *         splitted into Sections
	 */
	public Document toDocument(File file, int sectionLength) {
		InputStream input = null;
		AutoDetectReader parser = null;
		List<Section> sections = new LinkedList<Section>();
		try {
			// open File and prepare
			input = new FileInputStream(file);
			parser = new AutoDetectReader(input);
			int lineCount = 0;
			int orderingNumber = 1;
			String currentLine = parser.readLine();
			StringBuilder sb = new StringBuilder();
			sections = new LinkedList<Section>();
			// double check, both loops read!
			while (currentLine != null) {
				// using modulo makes splitting easy
				while (currentLine != null
						&& lineCount % sectionLength != sectionLength - 1) {
					sb.append(currentLine + " ");
					currentLine = parser.readLine();
					lineCount++;
				}
				// we're done with a Section, create it and prepare for next
				// Section.
				sections.add(new Section(sb.toString(), "", file
						.getAbsolutePath(), orderingNumber++));
				sb.setLength(0);
				currentLine = parser.readLine();
				lineCount++;
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block //NONE of these should ever occur
			// here!
			e.printStackTrace(); // Check document validity on upload!
		} catch (IOException e) { // If anything goes wrong here, application is
									// probably corrupt!
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TikaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (input != null)
				try {
					input.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (parser != null)
				try {
					parser.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		Document document = new Document(sections);
		return document;
	}
}
