package main
import akka.actor._
import convert.Document
import java.io.File
/**
 * Provides props and the messages sent by this Actor
 */
object ServerActor {
  def props(convertActor: ActorRef): Props = Props(new ServerActor(convertActor))
  case class ConvertThisMessage(document: File)
}

/**
 * Receives the messages from the WebApplication.
 * Starts the pipeline by sending a message to the first actor in the chain.
 */
class ServerActor(convertActor: ActorRef) extends Actor {
  def receive = {
    //case FileUploadedMessage(message) => convertActor ! ConvertThisMessage(message.Path)
    case _ =>
  }
}