package main
import akka.actor._
import com.typesafe.config
import com.typesafe.config.ConfigFactory
import compare.CompareActor
import convert.ConvertActor
import extract.ExtractActor
import saveHTML.SaveHtmlActor
import de.dhbw.citeright.server.search.SearchActor
import akka.event.Logging

/**
 * The main entry Point for the Application
 * Creates the Actorsystem the Application relies on
 * and starts it's top-level actors
 */
object WorkloadServer extends App {
  //Create System
  val system = ActorSystem.create("WorkloadServer", ConfigFactory load "server.conf")
  //Create all Actors to be accessible by url
  val compare = system.actorOf(Props create classOf[CompareActor], "compare")
  val convert = system.actorOf(Props create classOf[ConvertActor], "convert")
  val extract = system.actorOf(Props create classOf[ExtractActor], "extract")
  val saveHtml = system.actorOf(Props create classOf[SaveHtmlActor], "saveHtml")
  val search = system.actorOf(Props create classOf[SearchActor], "search")

  val server = system.actorOf(ServerActor.props(convert), "server")

  //omitting system.shutdown to keep the program running
}

