package utility
import akka.actor._

object ReboundMockActor {
  def props(): Props = Props(new ReboundMockActor())

  case class MockMessage(mockedOne: Props, messageToTell: AnyRef)
}

class ReboundMockActor() extends Actor {
  import ReboundMockActor._
  var rebound: ActorRef = context.actorOf(props) //hack to make it possible to save the first sender
  def receive = {
    case MockMessage(toMock, message) =>
      context.actorOf(toMock) ! message
      rebound = sender
    case x => rebound ! x
  }

}