package srcPackages.saveHtmlTest;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import saveHTML.SaveHtmlActor;
import scala.collection.JavaConversions;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import utility.ReboundMockActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.pattern.Patterns;
import akka.util.Timeout;
import de.dhbw.citeright.server.search.SearchActor.DocumentSearchResultsMessage;
import de.dhbw.citeright.server.search.SearchActor.SectionResults;
import de.dhbw.citeright.server.search.SearchResult;
import extract.Section;

public class SaveHtmlTest {
	@Test
	public void testActor() {
		// prepare
		ActorSystem system = ActorSystem.create();
		ActorRef mockedReceiver = system.actorOf(ReboundMockActor.props());

		// create test data
		List<SearchResult> searchResults = new LinkedList<SearchResult>();
		searchResults.add(new SearchResult("test", "http://test.com"));
		searchResults.add(new SearchResult("assert", "http://assert.com"));
		Section section = new Section("Test", "", "Test", 1);
		List<SectionResults> documentResults = new LinkedList<SectionResults>();
		documentResults.add(new SectionResults(section, JavaConversions
				.asScalaBuffer(searchResults)));
		documentResults.add(new SectionResults(section, JavaConversions
				.asScalaBuffer(searchResults)));
		DocumentSearchResultsMessage mockedMessage = new DocumentSearchResultsMessage(
				JavaConversions.asScalaBuffer(documentResults));

		// send message
		Timeout timeout = new Timeout(Duration.create(5, "seconds"));
		Future<Object> replyOfSave = Patterns.ask(
				mockedReceiver,
				new ReboundMockActor.MockMessage(SaveHtmlActor
						.props(mockedReceiver), mockedMessage), timeout);
		Object result = null;
		try {
			result = Await.result(replyOfSave, Duration.create(10, "seconds"));
		} catch (Exception e) {
			fail();
		}

		// assert that result is valid
		assertEquals(SaveHtmlActor.ConvertSearchResultsMessage.class,
				result.getClass());

	}

	private void fail() {
		// TODO Auto-generated method stub

	}
}
