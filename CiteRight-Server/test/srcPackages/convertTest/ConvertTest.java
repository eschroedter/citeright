package srcPackages.convertTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import main.ServerActor.ConvertThisMessage;

import org.junit.Ignore;
import org.junit.Test;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import utility.ReboundMockActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.pattern.Patterns;
import akka.util.Timeout;
import convert.ConvertActor;
import convert.Document;
import extract.Section;

public class ConvertTest {

	//TODO define Datatypes we get from the DB
	@Ignore @Test // will crash marvelously!
	public void testActor() {
		// prepare
		ActorSystem system = ActorSystem.create();
		ActorRef mockedReceiver = system.actorOf(ReboundMockActor.props());

		// create test data
		List<Section> sections = new LinkedList<Section>();
		sections.add(new Section("Test", "Test", "Test", 1));
		sections.add(new Section("Assert", "Assert", "Assert", 2));
		Document document = new Document(sections);
		ConvertThisMessage mockedMessage = new ConvertThisMessage(new File(""));

		// send message
		Timeout timeout = new Timeout(Duration.create(10, "seconds"));
		Future<Object> replyOfConvert = Patterns.ask(
				mockedReceiver,
				new ReboundMockActor.MockMessage(ConvertActor
						.props(mockedReceiver), mockedMessage), timeout);
		Object result = null;
		try {
			result = Await.result(replyOfConvert,
					Duration.create(10, "seconds"));
		} catch (Exception e) {
			fail();
		}

		// assert that result is valid
		assertEquals(ConvertActor.ConvertedDocumentMessage.class,
				result.getClass());

	}
}