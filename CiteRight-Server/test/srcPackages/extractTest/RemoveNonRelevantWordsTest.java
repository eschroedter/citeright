package srcPackages.extractTest;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import org.junit.Test;

import extract.RemoveNonRelevantWords;
import extract.Section;

public class RemoveNonRelevantWordsTest {

	@Test
	public void testIfRemoved() throws Exception {
		// prepare
		List<String> nonRelevants = new ArrayList<String>();
		nonRelevants.add("ist");
		nonRelevants.add("es");
		nonRelevants.add("so");

		String content = "Dieser Artikel ist sowas von plagiiert, dass es an ein Wunder grenzt, dass es noch niemand gemerkt hat. Wie wäre es denn mit einer Suche im Internet, um herauszufinden, ob er nicht schon irgendwo steht? Naja egal, es müssen erst einmal ein par Wörter gelöscht werden.";
		String removed = "Dieser Artikel sowas von plagiiert, dass an ein Wunder grenzt, dass noch niemand gemerkt hat. Wie wäre denn mit einer Suche im Internet, um herauszufinden, ob er nicht schon irgendwo steht? Naja egal, müssen erst einmal ein par Wörter gelöscht werden.";

		Section section = new Section(content,
				"Artikel+plagiiert+Wunder+yolo+irrelevant", "document", 1);

		RemoveNonRelevantWords remover = new RemoveNonRelevantWords(section,
				nonRelevants);

		ExecutorService system = Executors.newFixedThreadPool(1);

		// test
		Future<Section> result = system.submit(remover);
		assertEquals(removed, result.get().getSectionText());

		// tidy up
		system.shutdown();
	}
}
