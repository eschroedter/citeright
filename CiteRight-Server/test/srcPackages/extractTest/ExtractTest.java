package srcPackages.extractTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import utility.ReboundMockActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.pattern.Patterns;
import akka.util.Timeout;
import convert.ConvertActor.ConvertedDocumentMessage;
import convert.Document;
import extract.ExtractActor;
import extract.NonRelevantWords;
import extract.RemoveNonRelevantWords;
import extract.Section;
import extract.WordFrequency;

public class ExtractTest {
	@Test
	public void testActor() {
		// prepare
		ActorSystem system = ActorSystem.create();
		ActorRef mockedReceiver = system.actorOf(ReboundMockActor.props());

		// create test data
		List<Section> sections = new LinkedList<Section>();
		sections.add(new Section("Test", "Test", "Test", 1));
		sections.add(new Section("Assert", "Assert", "Assert", 2));
		Document document = new Document(sections);
		ConvertedDocumentMessage mockedMessage = new ConvertedDocumentMessage(
				document);

		// send message
		Timeout timeout = new Timeout(Duration.create(500, "seconds"));
		Future<Object> replyOfExtract = Patterns.ask(
				mockedReceiver,
				new ReboundMockActor.MockMessage(ExtractActor
						.props(mockedReceiver), mockedMessage), timeout);
		Object result = null;
		try {
			result = Await.result(replyOfExtract,
					Duration.create(1000, "seconds"));
		} catch (Exception e) {
			fail();
		}

		// assert that result is valid
		assertEquals(ExtractActor.SearchPhrasesMessage.class, result.getClass());

	}
	
	@Test
	public void testWordFrequency() {
		
		WordFrequency myWordFrequency = new WordFrequency();
		
		// create a string and test its' frequency
		String textRecord = "Vier Vier Drei Eins Zwei Vier Vier Drei Drei Zwei";
		List<String> result = new ArrayList<String>();
		result = myWordFrequency.getWordsInDescendingFreqOrder(textRecord);
		
		// check if the list is correct
		assertEquals("vier", result.get(0));
		assertEquals("drei", result.get(1));
		assertEquals("zwei", result.get(2));
		assertEquals("eins", result.get(3));

	}
	
	@Test
	public void testNonRelevantWords() {

		List<String> listWithRemovableWords = new ArrayList<>();
		listWithRemovableWords.add("der");
		listWithRemovableWords.add("die");
		listWithRemovableWords.add("das");		
		// create Instance of NonRelevantWords with the list of removable words
		NonRelevantWords myNonRelevantWords = new NonRelevantWords(listWithRemovableWords);
		
		// get List from NonRelevantWords
		List<String> testListWithRemovableWords = new ArrayList<>();
		testListWithRemovableWords.addAll(myNonRelevantWords.getAll());
		
		// check if the list is correct
		assertEquals("der", testListWithRemovableWords.get(0));
		assertEquals("die", testListWithRemovableWords.get(1));
		assertEquals("das", testListWithRemovableWords.get(2));

	}
	
	@Test
	public void testRemoveNonRelevantWords() {
		
		// prepare a list with removable words
		List<String> listWithRemovableWords = new ArrayList<>();
		listWithRemovableWords.add("der");
		listWithRemovableWords.add("die");
		listWithRemovableWords.add("das");
		listWithRemovableWords.add("Der");
		listWithRemovableWords.add("Die");
		listWithRemovableWords.add("Das");
		NonRelevantWords myNonRelevantWords = new NonRelevantWords(listWithRemovableWords);
		
		//
		Section mySection = new Section("hier ist mein text der Die das test", "defaultHeadword", "defaultDocument", 2);
		RemoveNonRelevantWords myRemoveNonRelevantWords = new RemoveNonRelevantWords(mySection, myNonRelevantWords.getAll());
		
		Section myOverWorkedSection = mySection;
		try {
			// get the over-worked section
			myOverWorkedSection = myRemoveNonRelevantWords.call();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// get attributes of the over-worked section and check if the attributes are correct
		assertEquals("hier ist mein text test", myOverWorkedSection.getSectionText());
		assertEquals("defaultHeadword", myOverWorkedSection.getHeadwords());
		assertEquals("defaultDocument", myOverWorkedSection.getDocument());
		assertEquals(2, myOverWorkedSection.getOrderingNumber());
		
	}
	
}
