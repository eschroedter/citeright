package srcPackages.searchTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import scala.collection.JavaConversions;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import utility.ReboundMockActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.pattern.Patterns;
import akka.util.Timeout;
import de.dhbw.citeright.server.search.SearchActor;
import extract.ExtractActor;
import extract.ExtractActor.SearchPhrasesMessage;
import extract.Section;

public class SearchTest {
	@Test
	public void testActor() {
		ActorSystem system = ActorSystem.create();
		ActorRef mockedReceiver = system.actorOf(ReboundMockActor.props());
		List<Section> sections = new LinkedList<Section>();
		sections.add(new Section("Test", "Test", "Test", 1));
		sections.add(new Section("Assert", "Assert", "Assert", 2));
		SearchPhrasesMessage mockedMessage = new ExtractActor.SearchPhrasesMessage(
				JavaConversions.asScalaBuffer(sections));

		Timeout timeout = new Timeout(Duration.create(500, "seconds"));
		Future<Object> replyOfSearch = Patterns.ask(
				mockedReceiver,
				new ReboundMockActor.MockMessage(SearchActor
						.props(mockedReceiver), mockedMessage), timeout);
		Object result = null;
		try {
			result = Await.result(replyOfSearch,
					Duration.create(1000, "seconds"));
		} catch (Exception e) {
			fail();
		}

		assertEquals(SearchActor.DocumentSearchResultsMessage.class,
				result.getClass());

	}

	@Test
	public void testSectionIsNull() {
		ActorSystem system = ActorSystem.create();
		ActorRef mockedReceiver = system.actorOf(ReboundMockActor.props());
		List<Section> sections = new LinkedList<Section>();
		sections.add(new Section("Test", "", "Test", 1));
		SearchPhrasesMessage mockedMessage = new ExtractActor.SearchPhrasesMessage(
				JavaConversions.asScalaBuffer(sections));

		Timeout timeout = new Timeout(Duration.create(500, "seconds"));
		Future<Object> replyOfSearch = Patterns.ask(
				mockedReceiver,
				new ReboundMockActor.MockMessage(SearchActor
						.props(mockedReceiver), mockedMessage), timeout);
		Object result = null;
		try {
			result = Await.result(replyOfSearch,
					Duration.create(1000, "seconds"));
		} catch (Exception e) {
			fail();
		}

		assertEquals(SearchActor.DocumentSearchResultsMessage.class,
				result.getClass());
	}

	@Test
	public void testRealSection() {
		ActorSystem system = ActorSystem.create();
		ActorRef mockedReceiver = system.actorOf(ReboundMockActor.props());
		List<Section> sections = new LinkedList<Section>();
		sections.add(new Section(
				"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
				"ipsum dolor sit amet+sadipscing elitr", "Test", 1));
		sections.add(new Section(
				"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
				"ipsum dolor sit amet+sadipscing elitr", "Test", 2));
		SearchPhrasesMessage mockedMessage = new ExtractActor.SearchPhrasesMessage(
				JavaConversions.asScalaBuffer(sections));

		Timeout timeout = new Timeout(Duration.create(500, "seconds"));
		Future<Object> replyOfSearch = Patterns.ask(
				mockedReceiver,
				new ReboundMockActor.MockMessage(SearchActor
						.props(mockedReceiver), mockedMessage), timeout);
		Object result = null;
		try {
			result = Await.result(replyOfSearch,
					Duration.create(1000, "seconds"));
		} catch (Exception e) {
			fail();
		}

		assertEquals(SearchActor.DocumentSearchResultsMessage.class,
				result.getClass());

	}
}
